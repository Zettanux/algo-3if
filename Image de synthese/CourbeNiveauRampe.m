function [carteNiveauCouleur] = CourbeNiveauRampe(terrain, inter, rampfile)

tailleTerrain = size(terrain);
h=tailleTerrain(1);
l=tailleTerrain(2);

carteNiveauCouleur = zeros(h,l,3);

%carteNiveau(abs( mod(terrain,inter) ) <= 0.1*inter,:) = 40

%Rescale de notre terrain � 490 pour simplifier la r�cup�ration de la
%couleur dans l'�chelle colormap
terrainRescale = reScale(terrain, 489);

for i = 1 : h
    for j = 1 : l
        if abs( mod(terrain(i,j),inter) ) <= 0.1*inter
            carteNiveauCouleur(i,j,:) = 40; % noir
        else
            %La couleur correspondante � l'�chelle
            val = ceil(terrainRescale(i,j))+1; % Car il peut y avoir des z�ros ! 
            carteNiveauCouleur(i,j,1) = rampfile(1,val,1);
            carteNiveauCouleur(i,j,2) = rampfile(1,val,2);
            carteNiveauCouleur(i,j,3) = rampfile(1,val,3);
        end
    end
end

carteNiveauCouleur = uint8(carteNiveauCouleur);
imwrite(carteNiveauCouleur, 'CourbeNiveauCouleur.png');

end