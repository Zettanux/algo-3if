function [] = RendreTerrainMatrix(terrain, texture,outfile,origin, target)

%% calcul de W 
w = [target(1) - origin(1);target(2) - origin(2);target(3) - origin(3)];
w = w / norm(w);

%% Calcul de u
up = [0; 0; 1]; 
u = cross(up, w); 
u = u/norm(u);

%% Calcul de v
v = cross(w, u); 

%% Calcul de matrice au compl�te
M = zeros(4); 
M(4,4) = 1; 
for i=1:3
    M(i, 1) = u(i,1);
    M(i, 2) = v(i,1); 
    M(i, 3) = w(i,1); 
    M(i, 4) = origin(i);
end

%% Reste de l'envoi
repertoire = '\\servif-home\fic_eleves\Espace Pedagogique\3IF\Modeles et Outils Mathematiques\Images fondements\image\Mitsuba';
cmd = strcat('"',repertoire,'\mitsuba" -Dhfile=',terrain,' -Dtfile=',texture,' -Dmatrix="',sprintf('%f ',M'), '" -o ',outfile,' texture-matrix.xml');
disp(cmd);
system(cmd);

end