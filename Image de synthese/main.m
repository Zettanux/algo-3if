%% On génère le terrain initial
Terr=[670   672   670   675   690   680   650   675   690   680   700   892   895;
    680   665   640   630   650   645   630   628   648   650   680   875   893;
    630   615   585   580   585   600   590   610   603   603   630   850   895;
    595   568   555   560   575   580   575   570   580   610   625   800   850;
    550   540   538   550   595   575   600   570   575   620   613   700   730;
    525   530   538   550   603   625   615   580   570   610   590   610   720;
    545   540   538   597   575   605   593   578   573   593   608   595   695;
    615   560   543   579   569   560   563   570   580   595   619   638   650;
    625   598   560   559   586   558   578   585   600   615   655   680   683;
    610   600   610   605   615   618   625   638   648   665   680   700   705];

%% D�finition de la lumi�re
lumiere = [-900, 1200, 200];


%% On agrandit le terrain par diamond square
[CN , TF] = GenererTerrain(Terr, 6, 10, 0.5);

%% On gère différents textures
%terrain, texture (ce qu'on applique), la ou enregistre notre fichier, camera, ou on regarde
% Rendu avec simple grille
RenderTerrain ( 'terrainFinal.png', 'grille.png', 'rendu.png', [-1200 ; -900 ; 600] , [0 ; 0 ; 200] );
% Rendu avec Courbe de niveau + grille
RenderTerrain ( 'terrainFinal.png', 'CourbeNiveau.png', 'renduCourbeNiveau.png', [0 ; -1100 ; 600] , [50 ; 0 ; 200] );
% Rendu avec couleurs + courbes de niveau + grille
RenderTerrain ( 'terrainFinal.png', 'CourbeNiveauCouleur.png', 'renduCourbeNiveauCouleurNew.png', [0 ; -1100 ; 600] , [50 ; 0 ; 200] );

%% On gère les ombrages
% Importation de la couleur
rampfile = imread('colorramp.png');

% On construit notre matrice de points (avec X, Y , Z comme valeurs de couches)
[ Points ] = ConstruitPoints3D(TF,-288,289,-384,385) ;
% TF fait 577 * 769, grossierement divise en deux, ca donne : -288 à 289 sur -384 à 385

% On construit notre ensemble de normales, à partir des points, en utilisant la méthode donnée
[ normales ] = ConstruitNormales( Points );

% On génère l'ombrage avec notre propre méthode
[carteOmbre] = Ombrage( TF, 10, rampfile, normales, Points, lumiere );
% Test avec cam�ra et lumi�re au m�me endroit.%[carteOmbre] = Ombrage( TF, 2000, rampfile, normales, Points, [-1000,0, 600] );

% En argument : le terrain, les lignes de niveau, le dégradé de couleurs, les normales, les points, l'emplacement de la lumière

%camera, le point regarde
RenderTerrain ( 'terrainFinal.png', 'carteOmbre.png', 'renduCarteOmbreAEux.png', [0 ; -1100 ; 600] , [50 ; 0 ; 200] );
% Test avec cam�ra et lumi�re au m�me endroit.%RenderTerrain ( 'terrainFinal.png', 'carteOmbre.png', 'renduCarteOmbre.png', [0 ; -1000 ; 600] , [0 ; 0 ; 0] );
RendreTerrainMatrix ( 'terrainFinal.png', 'carteOmbre.png', 'renduCarteOmbreANous.png', [0 ; -1100 ; 600] , [50 ; 0 ; 200] ); %Notre matrice de transformation

%% Affichage qui pourrait être faire.
% %% Plot
% figure;
% nbLigne = 4;
% nbCol = 1;
%
% subplot(nbLigne,nbCol,1);
% plot(nValsDeBase,abs(X));
% title('FFT avec symétrie sans FFTShift');
% xlabel('Points d''échantillonage (N-point DFT)')
% ylabel('Valeurs de la DFT');
%
% subplot(nbLigne,nbCol,2);
% plot(nValsNormalized,abs(X));
% title('FFT avec symétrie sans FFTShift');
% xlabel('Fréquences normalisées')
% ylabel('Valeurs de la DFT');