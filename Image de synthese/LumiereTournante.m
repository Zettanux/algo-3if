%% On génère le terrain initial
Terr=[670   672   670   675   690   680   650   675   690   680   700   892   895;
    680   665   640   630   650   645   630   628   648   650   680   875   893;
    630   615   585   580   585   600   590   610   603   603   630   850   895;
    595   568   555   560   575   580   575   570   580   610   625   800   850;
    550   540   538   550   595   575   600   570   575   620   613   700   730;
    525   530   538   550   603   625   615   580   570   610   590   610   720;
    545   540   538   597   575   605   593   578   573   593   608   595   695;
    615   560   543   579   569   560   563   570   580   595   619   638   650;
    625   598   560   559   586   558   578   585   600   615   655   680   683;
    610   600   610   605   615   618   625   638   648   665   680   700   705];

%% On agrandit le terrain par diamond square
[CN , TF] = GenererTerrain(Terr, 6, 10, 0.5);

%% On gère les ombrages
% Importation de la couleur
rampfile = imread('colorramp.png');

% On construit notre matrice de points (avec X, Y , Z comme valeurs de couches)
[ Points ] = ConstruitPoints3D(TF,-288,289,-384,385) ;
% TF fait 577 * 769, grossierement divise en deux, ca donne : -288 à 289 sur -384 à 385

% On construit notre ensemble de normales, à partir des points, en utilisant la méthode donnée
[ normales ] = ConstruitNormales( Points );

%% Génération de plusieurs textures, pour chaque
offset = [0;0;0];
pointCourantLumiere = [0;0;0];
pointCourantCamera = [0,-1000,600];
pointVisee = [50 ; 0 ; 200];

RayondeTournage = 2500;
nomRendu = 'RenduLumiereTourant/renduLumiereTourantN';  %Il faut que le dossier soit déjà créé ! Vidéo créée ensuite avec Fiji

for i=0:0.1:pi %On va tourner de 0 à Pi
    pointCourantLumiere(2) = offset(2) + cos(i)*RayondeTournage;
    pointCourantLumiere(3) = offset(3) + sin(i)*RayondeTournage;
    % On génère l'ombrage avec notre propre méthode
    [carteOmbre] = Ombrage( TF, 10, rampfile, normales, Points, pointCourantLumiere );
    RendreTerrainMatrix ( 'terrainFinal.png', 'carteOmbre.png', strcat(nomRendu, num2str(i), '.png'), pointCourantCamera , pointVisee);
end
% En argument : le terrain, les lignes de niveau, le dégradé de couleurs, les normales, les points, l'emplacement de la lumière

%% Affichage qui pourrait être fait.