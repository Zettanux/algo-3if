function [terrainRescale] = reScale(terrain, FuturMax)

%R�cup�re les bornes actuelles
Min = min(min(terrain));
Max = max(max(terrain));

%R�cup�re taille terrain
tailleTerrain = size(terrain);
h=tailleTerrain(1);
l=tailleTerrain(2);

terrainRescale = (terrain-Min) ./ (Max-Min) .* FuturMax;

end