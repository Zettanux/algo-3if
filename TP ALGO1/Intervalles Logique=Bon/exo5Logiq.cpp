#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

int main() {
	int nbLigne, nbValides;
	cin >> nbLigne;
	nbValides = 2*nbLigne;
	
	int val[2*nbLigne];
	bool table[2*nbLigne];
	
	for(int i=0; i<2*nbLigne ; i++){
		table[i] = true;
	}
	

	int result[nbValides];
	int indexCourant = 0;
	
	for(int i=0; i<2*nbLigne ; i++){
		if(table[i]==true){
			result[indexCourant] = val[i];
			indexCourant += 1;
		}
	}
	
	cout << nbValides/2 << "\r\n";
	for(int i=0; i<nbValides ;i= i+2){
		cout << result[i] << " " << result[i+1] << "\r\n";
	}

	return 0;
}
