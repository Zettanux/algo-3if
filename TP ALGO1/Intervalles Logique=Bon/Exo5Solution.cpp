#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

int main() {
	int nbLigne, nbValides;
	cin >> nbLigne;
	nbValides = 2*nbLigne;
	
	int val[2*nbLigne];
	bool table[2*nbLigne];
	
	for(int i=0; i<2*nbLigne ; i++){
		table[i] = true;
	}
	
	for(int i=0; i<nbLigne ;i++){
		scanf("%d %d",&val[2*i],&val[2*i+1]);
	}

	for(int i=1; i<2*nbLigne ; i++){
		if(val[i] >= val[i+1] && table[i]==true && table[i+1]==true){
			table[i] = false;
			table[i+1] = false;
			nbValides -= 2;
		}
		// DEBUG // cout << "Je vais jusqu'à l'indice " << i << endl;
	}
	
	// DEBUG // 
	/*
	for(int i=0; i<2*nbLigne ;i++){
		cout << table[i] << "\r\n";
	}
	// DEBUG // 
	* */
	
	int result[nbValides];
	int indexCourant = 0;
	
	for(int i=0; i<2*nbLigne ; i++){
		if(table[i]==true){
			result[indexCourant] = val[i];
			indexCourant += 1;
		}
	}
	
	cout << nbValides/2 << "\r\n";
	for(int i=0; i<nbValides ;i= i+2){
		cout << result[i] << " " << result[i+1] << "\r\n";
	}

	return 0;
}
