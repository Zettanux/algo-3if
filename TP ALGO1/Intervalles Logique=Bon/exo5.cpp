#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

int main() {
	int nbLigne, nbLigneRes;
	cin >> nbLigne;

	int val[nbLigne][2];
	int result[nbLigne][2];

	for(int i=0; i<nbLigne ;i++){
		scanf("%d %d",&val[i][0],&val[i][1]);
	}

	nbLigneRes = 0;
	result[0][0] = val[0][0];
	for(int i = 0; i<nbLigne-1 ;i++){
		if(val[i][1]>val[i+1][0]){
			//cout << val[i][1] << " sup à " << val[i+1][0] << "\r\n";
			result[nbLigneRes][1] = val[i+1][1];
		} else if (val[i][1]<val[i+1][0]){
			//cout << val[i][1] << " inf à " << val[i+1][0] << "\r\n";
			result[nbLigneRes+1][0] = val[i+1][0];
			nbLigneRes++;
		}
	}
	//cout << result[nbLigneRes][1] << " rempli par " << val[nbLigne][1] << "\r\n";
	result[nbLigneRes][1] = val[nbLigne-1][1];


	cout << nbLigneRes+1 << "\r\n";
	for(int i=0; i<nbLigneRes+1 ;i++){
		cout << result[i][0] << " " << result[i][1] << "\r\n";
	}

	return 0;
}
