DS Algorithmique - 3IF - 19 mai 2016
----------------------------------------

Contraintes
-----------
Les solutions au DS sont valid�es via la plateforme domjudge. Chaque �l�ve a 
son propre login et son propre mot de passe. Vous pouvez envoyer plusieurs 
fois une solution pour un m�me probl�me sans aucune p�nalit�.

Vous avez le droit aux supports des cours, TD, TP et aussi � des ressources 
Internet, mais vous n'avez pas le droit de communiquer avec d'autres personnes. 
La DSI sauvegarde tout le trafic TCP/IP pendant le DS. Vous pouvez utiliser 
seulement le protocole http pour acc�der aux diff�rents sites Web. Une 
connexion � un outil de messagerie, r�seau social etc invalide automatiquement
votre participation au DS avec les cons�quences pr�vues par le r�glement des 
�tudes.

Vous pouvez utiliser le langage C ou le C++, mais sans utiliser la STL.

Probl�me 2 : sous-s�quence contig�e de longueur maximale
-----------------------------------------------------

Pour une s�quence de n nombres r�els (0 < n <= 5000), nous vous demandons de trouver
la longueur maximale d'une sous-s�quence contig�e de nombres identiques. 
Exemple 1 : pour la s�quence 1 -2 10 10 10 5 5 1 le r�sultat 
est 3 (correspondant � la sous-s�quence 10 10 10).

Exemple 2 : pour la s�quence 1 -10 10 -2 -2 5 5 le r�sultat 
est 2.

Observation : pour ce probl�me vous pouvez obtenir une erreur de type TIMELIMIT,
correspondant � une dur�e d'ex�cution trop importante de votre algorithme !

Format d'entr�e
---------------

L'entr�e standard sera compos�e d'une s�rie de nombres :
- un nombre entier indiquant le nombre de valeurs � analyser not� n
- n nombres r�els correspondants aux valeurs � analyser

Le format en entr�e est respect�, vous n'avez pas � faire de test pour le v�rifier.

Format de sortie
----------------

Un nombre r�el correspondant � la valeur recherch�e.


Exemple
-------

Entr�e :
8
1
-2
10
10
10
5
5
1

Sortie :
3

Lecture/affichage de donn�es
----------------------------
Pour lire un entier :

	#include <stdio.h>
	...
	int n ;
	fscanf(stdin,"%d",&n);

Pour lire un flottant :
	float fValue = 0;
	fscanf(stdin,"%f",&fValue);

Pour afficher un entier :
	printf("%d", n);
