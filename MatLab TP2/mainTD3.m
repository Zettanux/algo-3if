%% On prend une matrice
A = [12, 3 ; 4 ,6];

%% On pr�pare le terrain
taille = size(A);
ValeurPropre = zeros(taille(1),1);

%% On calcul toutes ses valeurs propres (= sa dimension)
for i=1:taille(1)
    [ValeurPropre(i,1), v, u] = PuissanceIteree(A);
    disp([ ' valeur propre trouv�e ', num2str(ValeurPropre(i))]);
    A = DeflationWielandt(A, ValeurPropre(i,1), v, u);
end

