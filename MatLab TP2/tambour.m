function [A] = tambour()

lignes = 16;
colonnes = 40;
nbPoints = lignes*colonnes;

A = zeros(nbPoints, nbPoints );

% Constituer les voisins "g�n�raux"
valeurCourant = 0;
for i=1:nbPoints
    %% Voisins du dessus
    if(i-1 >0 && mod(i-1, 16)~=0) %Si on sort pas du tableau et qu'on est pas sur la ligne du haut (cad si i-1 n'est pas sur la ligne du bas)
        A(i,i-1) = 1 ; %Celui juste au dessus = n-1
        valeurCourant = valeurCourant +1;
    end
  
    %% Voisins du dessous
    if(i+1 < nbPoints && mod(i, 16)~=0) %Si on sort pas du tableau et qu'on est pas sur la ligne du bas (i modulo 16)
        A(i,i+1) = 1 ; %Celui juste en dessous = n+1
        valeurCourant = valeurCourant +1;
    end
    
    %% Voisins de gauche
    if(i-16 > 0) %Si on sort pas du tableau = premi�re colonne
        A(i,i-16) = 1 ; %Celui 16 colonne � gauche = n-16
        valeurCourant = valeurCourant +1;
    end
    
    %% Voisins de droite
    if(i+16 <= nbPoints) %Si on sort pas du tableau sauf si derni�re colonne
        A(i,i+16) = 1 ; %Celui 16 colonne � droite = n+16
        valeurCourant = valeurCourant +1;
    end
    
    A(i,i) = - valeurCourant; %On part de celui de la diagonale (donc lui m�me)
    valeurCourant = 0;  %On remet la valeur courante � 0
end

%% Points sp�ciaux
valeurDeRemiseAZero = 1;

% Point haut gauche
A(1, :) = 0; % On remet les lignes � 0
A(1, 1) = valeurDeRemiseAZero;


for i=1:lignes %derni�re colonne
    A(round(nbPoints+1-i), :) = 0; % On remet les lignes � 0
    A(round(nbPoints+1-i), round(nbPoints+1-i)) = valeurDeRemiseAZero; % On met la valeur sur la diagonale � la valeur choisie
end

for i=1:nbPoints 
    if mod(i-1, 16)==0 && i>209 %209=point du bas pour la 14eme colonne(13) on est sur la ligne du haut 
        A(i, :) = 0; % On remet les lignes � 0
        A(i, i) = valeurDeRemiseAZero;
    end
    if mod(i, 16)==0 %on est sur la ligne du bas 
        A(i, :) = 0; % On remet les lignes � 0
        A(i, i) = valeurDeRemiseAZero;
    end
    if mod(i+4, 16)==0 && i<304 && i>144  %304=point du bas pour la 19eme colonne; 144=point du bas pour la 10eme colonne(9)
        A(i, :) = 0; % On remet les lignes � 0
        A(i, i) = valeurDeRemiseAZero;
    end
    
end

%S = reshape(A,[lignes,colonnes]);

end