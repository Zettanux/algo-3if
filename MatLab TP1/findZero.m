function [] = findZero(A, ligne)

taille = size(A);

for n=1:taille(2)
    if(A(ligne,n)~=0)
        disp(['Colonne : ', num2str(n) , ' valeur : ' , num2str(A(ligne,n))]) ;
    end
end

end