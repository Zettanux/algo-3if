function [X] = ExponentielleStableCST(A, B, iterations, deltaTemps, epsilon)
tic
U = B;
saut = expm(A*deltaTemps);

Uprecedent = ones(1600,1);
n = 0;

%% Calcul des lieux des 100�c
lignes = 16;
colonnes = 100;
IndexCentDegresInf = round(lignes*colonnes*0.8+0.6*lignes);
IndexCentDegresSup = round(lignes*colonnes*0.81+0.6*lignes);

while (n<iterations) && ((max(abs(Uprecedent-U)))>epsilon),
    Uprecedent = U;
    U = saut* U;
    n = n+1;
    
    % remise � 100�C
    U(IndexCentDegresInf, 1) = 100;
    U(IndexCentDegresInf+1, 1) = 100;
    U(IndexCentDegresSup, 1) = 100;
    U(IndexCentDegresSup+1, 1) = 100;
end

disp([ ' Stabilit� trouv�e au rang : ' , num2str(n)]);
disp([ 'Temp�rature moyenne : ' ,  num2str(mean(mean(U)))]);

S = reshape(U,[16,100]);
surf(S)
axis([0 100 0 16 0 100]);
toc
end