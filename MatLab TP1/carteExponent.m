function [A, S, B] = carteExponent()
lignes = 16;
colonnes = 100;
nbPoints = lignes*colonnes;

%% une grande matrice avec pleins de points
B=zeros(nbPoints, 1);

% On rajoute les 4 points � 100�
IndexCentDegresInf = round(lignes*colonnes*0.8+0.6*lignes);
IndexCentDegresSup = round(lignes*colonnes*0.81+0.6*lignes);
B(IndexCentDegresInf, 1) = 100;
B(IndexCentDegresInf+1, 1) = 100;
B(IndexCentDegresSup, 1) = 100;
B(IndexCentDegresSup+1, 1) = 100;

%% cr�ation de la matrice X
X = zeros(nbPoints, 1);

%% cr�ation de la matrice A
A = zeros(nbPoints, nbPoints);

% Constituer les voisins "g�n�raux"
valeurCourant = 0;
for i=1:nbPoints
    %Celui juste au dessus de lui = -1 MAIS si on est � un multiple de 16
    %au quel cas on a 2 choix : soit inf�rieur � 160 soit sup�rieur � 160
    
    % Sur les "normaux" sauf ligne du haut, voisins du dessus // OK !
    if(i-1 >0 && mod(i-1, 16)~=0) %Si on sort pas du tableau et qu'on est pas sur la ligne du haut (cad si i-1 n'est pas sur la ligne du bas)
        A(i,i-1) = 1 ; %Celui juste au dessus = n-1
        valeurCourant = valeurCourant +1;
    end
   
    % Sur la bordure "recoll�e" haut gauche// OK !
    if(i-1 >0 && mod(i-1, 16)==0 && i <= lignes*0.1*colonnes+1) %Si on sort pas du tableau et qu'on est pas sur la ligne du haut (cad si i-1 n'est pas sur la ligne du bas)
        indiceSaut  = lignes*colonnes-(lignes*0.1*colonnes +1 - i);
        A(i,round(indiceSaut)) = 1 ; %Saut � celui tout � la fin : 1600 - (161- 161) � 1600 - (161 - 1)
        valeurCourant = valeurCourant +1;
    end
    
    % Sur les "normaux" sauf ligne du bas voisins du dessous// OK !
    if(i+1 < nbPoints && mod(i, 16)~=0 && i<lignes*0.9*colonnes) %Si on sort pas du tableau et qu'on est pas sur la ligne du bas (i modulo 16)
        A(i,i+1) = 1 ; %Celui juste en dessous = n+1
        valeurCourant = valeurCourant +1;
    end
    % Sur la bordure "recoll�e" bas droite // OK !
    if(i+1 < nbPoints && mod(i, 16)==0 && i>=lignes*0.9*colonnes) %Si on sort pas du tableau et qu'on est pas sur la ligne du bas (i modulo 16)
        indiceSaut  = i -(lignes*0.89*colonnes - 1); %Diff�rence ce 0.01% � cause des fins/d�buts / exclus/inclus
        % 17 est reli� � 1440 et 161 est reli� � 1584
        A(i,round(indiceSaut)) = 1 ; %Celui tout au d�but, qui est son voisin du dessus : VoisinDuDessus = 1600 - 1423 = i - lignes*0.9*colonnes +1
        valeurCourant = valeurCourant +1;
    end
    
    %Voisins de gauche
    if(i-16 > 0) %Si on sort pas du tableau = premi�re colonne
        A(i,i-16) = 1 ; %Celui 16 colonne � gauche = n-16
        valeurCourant = valeurCourant +1;
    end
    %Voisins de droite
    if(i+16 <= nbPoints) %Si on sort pas du tableau sauf si derni�re colonne
        A(i,i+16) = 1 ; %Celui 16 colonne � droite = n+16
        valeurCourant = valeurCourant +1;
    end
    
    A(i,i) = - valeurCourant; %On part de celui de la diagonale (donc lui m�me)
    valeurCourant = 0;  %On remet la valeur courante � 0
end

%% reshape des matrices pour mieux voir

S = reshape(B,[lignes,colonnes]);

end