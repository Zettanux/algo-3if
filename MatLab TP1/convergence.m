function [rep] = convergence(A)
% Test de diagonale dominante pour la convergence
taille = size(A);
rep = 0;
somme =0;

if( det(A) ~= 0)
    for i=1:taille(1),%i=k dans cours
        for j=1:taille(2), %j=i dans cours
            if(i~=j)
                somme = somme + abs(A(i,j));
            end
        end
        if(somme >= abs(A(i,i)))
            rep = 1;
        end
        somme = 0 ;
    end
end

end