function [L, D, U] = decompos(A)

taille = size(A);
L = zeros(taille(1));
D = zeros(taille(1));
U = zeros(taille(1));

for i=1:taille(1),
    D(i, i) = A(i, i);
end
for i=2:taille(1),
    for j=1:i-1,
        L(i, j) = A(i, j);
    end
end
for i=1:taille(1),
    for j=i+1:taille(1),
        U(i, j) = A(i, j);
    end
end
end