function [X] = Gauss(A, B, iterations)
%m�thode Gauss Seidel

taille = size(A);
Xsuivant = zeros(taille(1),1);
X = zeros(taille(1),1);
sommeX = 0; %x(m+1) en cours
sommeConnus = 0; %x(m)

for n=1:iterations
    for i=1:taille(1),
        for j=i+1:taille(1),
            sommeConnus = sommeConnus + A(i, j)*X(j, 1);
        end
        if i~=1
            for j=1:i-1,
                sommeX = sommeX + A(i, j)*Xsuivant(j, 1);
            end
        end
        Xsuivant(i, 1) = B(i, 1) - sommeConnus - sommeX;
        Xsuivant(i, 1) =  Xsuivant(i, 1)/A(i,i);
        sommeConnus = 0;
        sommeX = 0;
    end
    X = Xsuivant;
end

end