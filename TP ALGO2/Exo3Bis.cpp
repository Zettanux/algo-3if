#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

int main() {
	int tailleSac, nbObjet = 0, valCourant=0;
	int taillesObjets[100];
	cin >> tailleSac;
	
	while(valCourant != -1){
		cin >> taillesObjets[nbObjet];
		valCourant = taillesObjets[nbObjet];
		nbObjet++;
		//DEBUG // cout << valCourant;
	}

	bool sac[tailleSac+1][nbObjet+1]; //Car il nous faut bien un "tailleDeSacième" empalcement et une première ligne pour oui ou non.
	sac[0][0] = true; // Oui, même pour le -1 qui ne sera jamais atteint
	
	for(int i = 1 ; i < tailleSac; i++){
		sac[i][0] = false;
		for(int j = 0; j<nbObjet-1 ; j++){
			if(taillesObjets[j]<=tailleSac-i && sac[i][j+1] == false){ //Un true dans un case objet vaut un "déjà utilisé". Commence à la ligne 1, pas 0.
				int indiceCaseCourante = i - taillesObjets[j]; 
				if( indiceCaseCourante >=0){ //On vérifie qu'on est pas en dehors du tableau
					if(sac[indiceCaseCourante][0] == true){
						sac[i][0] = true; //On note la case comme atteinte
						for(int k=1 ; k<nbObjet-1 ; k++){
							sac[i][k] = sac[indiceCaseCourante][k]; // On ramène tous les status des objets précédents
						}
						sac[i][j+1] = true; //On marque l'objet j comme déjà utilisé.
					}
				}
			}
		}
		//DEBUG // 		cout << sac[i];
	}
	
	if(sac[tailleSac-1][0] == true){ //si la dernière case est atteinte
		cout << "OUI" << "\r\n";
	} else {
		cout << "NON" << "\r\n";
	}

	return 0;
}
