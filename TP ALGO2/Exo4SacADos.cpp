#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

int main() {
	int capacite, nbObjets = 0 ,valCourante = 0;
	int listeObjets[100];
	cin >> capacite;
	
	while(valCourante != -1){
		cin >> valCourante;
		if(valCourante != -1){
			listeObjets[nbObjets] = valCourante ;
			nbObjets++;
		}
		//DEBUG // cout << valCourant;
	}

	//Declaration du tableau de boolean avec capacite+1 case pour le zéro
	bool sac[capacite+1];
	
	// Initialisation du sac : true pour 0 false pour le reste
	for(int j = 1 ; j <= capacite ; j++){
		sac[j] = false;
	}
	sac[0] = true;
	
	for(int i = 0; i<nbObjets ; i ++){ //pour chaque objet
		for(int j = capacite ; j >= 0 ; j--){ //Pour chaque case du tableau de booléan
			if(sac[j] == true && listeObjets[i]+j <= capacite){ //Si on tombe sur une case accessible, et qu'on ne sort pas du tableau
				sac[listeObjets[i]+j] = true; // On pose un "true" pour dire que la case est accessible.
			}
			// DEBUG // cout << sac[j]; // Affichage de chaque "point" dans la ligne /!\ affichage inversé ! (le début est à droite !)
		}
		//DEBUG // cout << endl; // pour mieux lire l'affichage précédant
	}
	
	if(sac[capacite] == true){
		cout << "OUI" << "\r\n";
	} else {
		cout << "NON" << "\r\n";
	}

	return 0;
}
