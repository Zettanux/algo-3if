#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

int main() {
	int tailleSac, nbObjet = 0, valCourant=0;
	int taillesObjets[100];
	cin >> tailleSac;
	
	while(valCourant != -1){
		cin >> taillesObjets[nbObjet];
		valCourant = taillesObjets[nbObjet];
		nbObjet++;
		//DEBUG // cout << valCourant;
	}

	bool sac[tailleSac+1][nbObjet+1]; //Car il nous faut bien un "tailleDeSacième" empalcement et une première ligne pour oui ou non.
	sac[0] = true;
	
	for(int i = 1 ; i < tailleSac; i++){
		sac[i] = false;
		for(int j = 0; j<nbObjet-1 ; j++){
			if(taillesObjets[j]<=tailleSac){
				int indiceCaseCourante = i - taillesObjets[j];
				if( indiceCaseCourante >=0){
					if(sac[indiceCaseCourante] == true){
						sac[i] = true;
					}
				}
			}
		}
		//DEBUG // 		cout << sac[i];
	}
	
	if(sac[tailleSac-1] == true){
		cout << "OUI" << "\r\n";
	} else {
		cout << "NON" << "\r\n";
	}

	return 0;
}
