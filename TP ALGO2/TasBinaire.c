#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  int allocated; /* current allocation of array */
  int filled;    /* number of items present in the binheap */
  int *array;    /* array of values */
} BinaryHeap;

/* Init allocates the structure BinaryHeap and
 * also the membre array with the given size 
 * it also fill allocated (size) and intializes 
 * filled to 0 */
BinaryHeap * Init(int size);

/* InsertValue insert value into the binary heap
 * the array is reallocated if necessary (allocated changed 
 * with respect to the new size )
 * filled is incremented by 1 */
void InsertValue(BinaryHeap * heap, int value);

/* ExtractMAx returns 0 if the binary heap is empty
 * otherwise it return 1 and fills *val with the maximum 
 * value present in the binary heap
 * filled is decremented by 1  and the max value is removed
 * from the binary heap */
int ExtractMax(BinaryHeap * heap, int * val);

/* Destroy frees the structure and the array */
void Destroy(BinaryHeap * heap);


int main(void) 
{
  char lecture[100];
  int val;
  BinaryHeap * heap;
  heap = Init(10);

  fscanf(stdin,"%99s",lecture);
  while (strcmp(lecture,"bye")!=0) {
    if (strcmp(lecture,"insert")==0) {
      fscanf(stdin,"%99s",lecture);
      val = strtol(lecture,NULL,10);
      InsertValue(heap,val);
    } else if (strcmp(lecture,"extract")==0) {
      if(ExtractMax(heap,&val))
      {
        printf("%d\r\n",val);
      }
    }
    fscanf(stdin,"%99s",lecture);
  }
  Destroy(heap);
  return 0;
}

BinaryHeap * Init(int size)
{
  BinaryHeap * heap;
  heap = (BinaryHeap*) malloc(sizeof(BinaryHeap)); //Systeme cherche tout seul la taille qu'il faut.
  heap->array = (int*) malloc(size * sizeof(int)); // On alloue le tableau dans la structure.
  heap->allocated = size; 
  heap->filled = 0;
  
  return heap;
}

void InsertValue(BinaryHeap * heap, int value)
{
  if(heap->allocated == heap->filled){
	int *newArray;
	newArray = (int*) malloc(((heap->allocated+10) * sizeof(int)));
	int i;
	for(i=0; i<heap->filled ;i++){
		newArray[i] = heap->array[i];
	}
	
	free(heap->array);
	heap->array = newArray;
	
	heap->allocated = heap->allocated+10;
  }
  
  heap->array[heap->filled] = value;
	heap->filled = heap->filled + 1;
	
	int indiceFils = (heap->filled - 1);
	int indicePere = (indiceFils-1)/2 ;
	
	while( indicePere >= 0 &&  heap->array[indicePere] < heap->array[indiceFils] ) {
		// tant que pas la racine ET Que le père Inférieur au fils .. on permute.
		int interm = heap->array[indicePere];
		heap->array[indicePere] = heap->array[indiceFils];
		heap->array[indiceFils] = interm;
		indiceFils = indicePere;
		indicePere = (indiceFils-1)/2 ;
	}
	//printf("VALEUR AJOUTEE");
  
}

int ExtractMax(BinaryHeap * heap, int *res) //Le pointeur est il alloué à ce stade, quand il est passé en argument ? 
{
  if(heap->filled != 0){
	 //printf("On extrait %d", heap->array[0]);
	 *res = heap->array[0];
	 heap->array[0] = heap->array[heap->filled - 1]; //On récupère la valeur la plus grande
	 heap->filled = heap->filled-1; // On réduit la "taille" du tableau
	 
	 int indice = 0;
	 int indiceGauche = indice*2+1;
	 int indiceDroit = indice*2+2; 
	 int finBoucle =0;
	 
	 while(finBoucle == 0){
		if(indiceGauche < heap->filled && indiceDroit < heap->filled) { // LEs deux élément sont présents
			if(heap->array[indiceGauche] < heap->array[indice] && heap->array[indiceDroit] < heap->array[indice]){
				finBoucle = 1; 
			} else if(heap->array[indiceGauche] >= heap->array[indiceDroit]){ // on part à gauche
				// On permute les valeurs
				int interm = heap->array[indice];
				heap->array[indice] = heap->array[indiceGauche];
				heap->array[indiceGauche] = interm;
				// On reclacule les indices
				indice = indiceGauche;
				indiceGauche = indice*2+1;
				indiceDroit = indice*2+2;
			} else if(heap->array[indiceGauche] < heap->array[indiceDroit]){ // On part à droite
				// On permute les valeurs
				int interm = heap->array[indice];
				heap->array[indice] = heap->array[indiceDroit];
				heap->array[indiceDroit] = interm;
				// On reclacule les indices
				indice = indiceDroit;
				indiceGauche = indice*2+1;
				indiceDroit = indice*2+2;
			}
			
			//Les deux éléments sont inférieurs à notre élément courant
		} else if (indiceGauche < heap->filled ){ // L'élément gauche est seul
			if(heap->array[indiceGauche] > heap->array[indice]){ // on part à gauche
				// On permute les valeurs
				int interm = heap->array[indice];
				heap->array[indice] = heap->array[indiceGauche];
				heap->array[indiceGauche] = interm;

				finBoucle = 1; // On est arrivé à la fin du tableau
			} else {
				finBoucle = 1; // On est arrivé à la fin du tableau
			}
			
			//L'élément gauche est inférieur à notre élément courant
		} else {
			finBoucle = 1; // On est arrivé à la fin du tableau
		}
	}
	return 1;
	} else {
	return 0;
	}
  
  
}

void Destroy(BinaryHeap * heap)
{
  free(heap->array);
  heap->allocated =0; 
  heap->filled =0;
  free(heap);
}

