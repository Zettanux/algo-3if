#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

int main() {
	int a,b, nbTrouve = 0;
	string result = "";
	
	cin >> a;
	cin >> b;
	
	for(int i=0; i<a; i++){
		for(int j=0; j<b ; j++){
			if( (i*i + j*j) == a && (i*i*i + j*j*j) == b ){
				cout << i << " " << j << "\r\n";
				nbTrouve ++;
			}
		}
	}
	if(nbTrouve==0){
		cout << "-" << "\r\n";
	}

	return 0;
}
