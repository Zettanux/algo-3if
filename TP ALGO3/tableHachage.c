	#include <stdio.h>
	#include <string.h>
	#include <stdlib.h>
	
	typedef char * Key; //Key définit un type correspondant à un pointeur sur caractere (ou plusieurs caracteres histoire de size)
	
	typedef struct {
	  char statut; 
	  Key cle; //cle est un pointeur sur caracteres
	  char * valeur;
	} cellule;
	
	typedef struct {
	  int taille;
	  int tailleUsed;
	  cellule *array;
	} tableHash;
	
	void error(void);
	
	tableHash * Init(int size);
	void InsertValue(tableHash * table, Key key, char* value);
	void DeleteValue(tableHash * table, Key key);
	void Request(tableHash * table, Key key);
	void Stats(tableHash * table);
	void Destroy(tableHash * table);
	
	int main(void) 
	{
	   tableHash * table;
	   int size;
	   char lecture[100];
	   Key key;
	   char * val;
	
	   if (fscanf(stdin,"%99s",lecture)!=1)
	      error();
	   while (strcmp(lecture,"bye")!=0)
	   {
	      if (strcmp(lecture,"init")==0)
	      {
	         if (fscanf(stdin,"%99s",lecture)!=1)
	            error();
	         size = atoi(lecture); //transforme string en int
	         table = Init(size);
	      }
	      else if (strcmp(lecture,"insert")==0)
	      {
	         if (fscanf(stdin,"%99s",lecture)!=1)
	            error();
	         key = strdup(lecture);
	         if (fscanf(stdin,"%99s",lecture)!=1)
	            error();
	         val = strdup(lecture);
			InsertValue(table,key,val);
	         
	      }
	      else if (strcmp(lecture,"delete")==0)
	      {
	         if (fscanf(stdin,"%99s",lecture)!=1)
	            error();
	         key = strdup(lecture);
			 DeleteValue(table, key);
	
	      }
	      else if (strcmp(lecture,"query")==0)
	      {
	         if (fscanf(stdin,"%99s",lecture)!=1)
	            error();
	         key = strdup(lecture);
	         
	         Request(table,key);
	         
	         /* Si on devait le faire correctement :  
	         char valRenvoye;
	         if(Request(table,key,&valRenvoye))
			 {
				printf("%s\r\n",valRenvoye);
			 }
			 */
			 
	         /* mettre ici le code de recherche et traitement/affichage du résultat */
	      }
	      else if (strcmp(lecture,"destroy")==0)
	      {
	         Destroy(table);
	      }
	      else if (strcmp(lecture,"stats")==0)
	      {
	         Stats(table);
	      }
	
	      if (fscanf(stdin,"%99s",lecture)!=1)
	         error();
	   }
	   return 0;
	}
	
	/* fonction de décalage de bit circulaire */
	unsigned int shift_rotate(unsigned int val, unsigned int n)
	{
	  n = n%(sizeof(unsigned int)*8);
	  return (val<<n) | (val>> (sizeof(unsigned int)*8-n));
	}
	
	/* fonction d'encodage d'une chaîne de caractères */
	unsigned int Encode(Key key)
	{
	   unsigned int i;
	   unsigned int val = 0;
	   unsigned int power = 0;
	   for (i=0;i<strlen(key);i++)
	   {
	     val += shift_rotate(key[i],power*7);
	     power++;
	   }
	   return val;
	}
	
	/* fonction de hachage simple qui prend le modulo */
	unsigned int hash(unsigned int val, unsigned int size)
	{
	   return val%size;
	}
	
	/* fonction de hachage complète à utiliser */
	unsigned int HashFunction(Key key, unsigned int size)
	{
	   return hash(Encode(key),size);
	}
	
	/* placer ici vos définitions (implémentations) de fonctions ou procédures */
	
	void error(void)
	{
	   printf("input error\r\n");
	   exit(0);
	}
	
	tableHash * Init(int size)
	{
	  tableHash * table;
	  table = (tableHash*) malloc(sizeof(tableHash));
	  table->array = (cellule*) malloc(size * sizeof(cellule));
	  table->taille = size; // Mettre un point ici ? 
	  table->tailleUsed = 0; 
	  int i=0;
	  for(i=0;i<size;i=i+1)
	  {
		  table->array[i].statut = 'v'; //v pour vide r remplacee s supprimee
	  }
	  
	  return table;
	}
	
	void InsertValue(tableHash * table, Key key, char * value)
	{
		int indice = HashFunction(key, table->taille);
		if (table->array[indice].statut != 'r') {
			table->array[indice].statut = 'r';
			table->array[indice].cle = key;
			table->array[indice].valeur = value;
			table->tailleUsed = table->tailleUsed +1;
		} else if(table->array[indice].statut == 'r' && !strcmp(table->array[indice].cle,key)){ // on comparie les deux clé, si égales, renvois 0 (on inverse donc la sortie)
			table->array[indice].valeur = value;	
		} else if( table->array[indice].statut == 'r' && table->tailleUsed != table->taille){ // reste 1 place dans le tableau
			int compteur = indice ;
			while(table->array[compteur].statut == 'r'){
				compteur = (compteur +1)%table->taille; 
			}
			table->array[compteur].statut = 'r';
			table->array[compteur].cle = key;
			table->array[compteur].valeur = value;
			table->tailleUsed = table->tailleUsed +1;
		}
	}
	
	void DeleteValue(tableHash * table, Key key)
	{
		int indice = HashFunction(key, table->taille);
		if(table->array[indice].statut == 'r')
		{
			table->array[indice].statut = 's';
			free(table->array[indice].cle); //liberer 
			free(table->array[indice].valeur);
			table->tailleUsed = table->tailleUsed -1;
		}
		//Si on rempli avec clé1 puis cle2, si on supprime avec clé1, doit on faire la vérification que clé1 est toujorus dans la case ? ^^
	}
	
	
	void Request(tableHash * table, Key key){
		
		int indice = HashFunction(key, table->taille);
		
		if(table->array[indice].statut == 'r' && !strcmp(table->array[indice].cle,key)){ // on comparie les deux clé, si égales, renvois 0 (on inverse donc la sortie)
			printf("%s\r\n",table->array[indice].valeur); //string affiche
		} else {
			printf("Not found\r\n");
		}
		// Note : on ne peut pas vraiment récupérer, ainsi, quelques chose qui a été 'déplacé' un cran plus loin dans la tableau. (collisions)
	}
	
	void Stats(tableHash * table){
		int used = 0;
		int deleted = 0;
		int i=0;
		
	  for(i=0;i<table->taille;i=i+1){
		  if(table->array[i].statut == 's'){
			  deleted++;
		  } else if(table->array[i].statut == 'r'){
			  used++; 
		  }
		}
	  	int empty = table->taille - (deleted+used);
	  	
	  printf("size    : %d\r\n",table->taille);
	  printf("empty   : %d\r\n",empty);
	  printf("deleted : %d\r\n",deleted);
	  printf("used    : %d\r\n",used);
	}
	
	
	void Destroy(tableHash * table)
	{
	  int i=0;
	  for(i=0;i<table->taille;i=i+1)
	  {
		if(table->array[i].statut == 'r') 
		{
			free(table->array[i].cle); 
			free(table->array[i].valeur);
		}
	  }
	  free(table->array);
	  free(table);
	}
	
	/* A revoir 
	int Request(tableHash * table, Key key, char *res)
	{
		int indice = HashFunction(key, table->taille);
		
		if(table->array[indice].statut == 'r')
		{
		  *res = *table->array[indice].valeur;
		  return 1;
	   } else {
		  return 0;
	   }
	   
	}
	*/
	
