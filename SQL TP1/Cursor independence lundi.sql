DECLARE
    -- Déclaration d'un curseur qui parcours COUNTRY
  CURSOR C_POLITICS IS
  SELECT COUNTRY,INDEPENDENCE
  FROM POLITICS;
  
    -- Nos variables
  var_country_code$ Politics.country%type;
  var_independence_date Politics.independence%type;
  var_independence_date_day VARCHAR2(45);
  var_country_name$ Country.name%type;

  BEGIN
  
  Open C_POLITICS;
  Loop --boucle sur les lignes du curseur
      Fetch C_POLITICS INTO var_country_code$,var_independence_date;
      
      var_independence_date_day := to_char(var_independence_date, 'D');
      --dbms_output.put_line('Date  ' || var_independence_date_day);

      IF var_independence_date_day = 1 THEN
        SELECT name INTO var_country_name$
        FROM Country
        WHERE code = var_country_code$;
        
        dbms_output.put_line('Nom du pays  ' || var_country_name$);
      END IF;
      
    EXIT When C_POLITICS%NOTFOUND; -- sortie quand on a plus de lignes
  End Loop;
  Close C_POLITICS;
  END; 
/