CREATE OR REPLACE TRIGGER t_politics_independance_day
  AFTER INSERT OR UPDATE
  ON POLITICS
  FOR EACH ROW
  DECLARE
    today_date DATE := SYSDATE;
  BEGIN

    --dbms_output.put_line('Date  ' || today_date);

    IF :NEW.independence > today_date THEN
      RAISE_APPLICATION_ERROR(-20000,'Independance DAY INVALID');
      -- Il faut normalement cr�er une table avec les erreurs et faire une s�lection sur cette table. ;)
    END IF;
    
    
  END;
/

--verfie le trigger precedent
SELECT independence
FROM POLITICS
WHERE country = 'AL';
-- 28/11/12

UPDATE POLITICS
SET independence = '10/08/2099'
WHERE country = 'AL';

UPDATE POLITICS
SET independence = '28/11/12'
WHERE country = 'AL';
--fin de verification du trigger

-- Alphanum�rique et majuscule
CREATE OR REPLACE TRIGGER t_country_code_alph_maj
  AFTER INSERT OR UPDATE
  ON COUNTRY
  FOR EACH ROW
  DECLARE
    var_longueur_code$ number ;
  BEGIN
     
    --dbms_output.put_line('Date  ' || today_date);
    var_longueur_code$ = LENGTH(:NEW.code)
    IF LENGTH(TRANSLATE(:NEW.code, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', '')) = var_longueur_code$ THEN
    --IF LENGTH(TRANSLATE(:NEW.code, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', '')) IS NOT NULL THEN
    --IF to_char(:NEW.code, 'AAA') THEN
      RAISE_APPLICATION_ERROR(-20001,'CODE INVALID');
      -- Il faut normalement cr�er une table avec les erreurs et faire une s�lection sur cette table. ;)
    END IF;
    
  END;
/

DECLARE
  BEGIN
  dbms_output.put_line(to_char('blabla', 'AAA'));
  END;
/
--verfie le trigger precedent
INSERT INTO Country
VALUES ('TESTCITY', 'K%x', 'TestCapitale', 'TestProvince', 1000, 1000, null);

INSERT INTO Country
VALUES ('TESTCITY2', 'ABC', 'TestCapitale2', 'TestProvince2', 10002, 10002, null);

UPDATE Country
SET code = 'APC'
WHERE code = 'K%x';

SELECT name, code
FROM Country
WHERE province = 'TestProvince';