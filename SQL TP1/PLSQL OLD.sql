set serveroutput on;
/

DECLARE
  
  var_country_code$ Country.Code%type;
  
  var_country_voisins1$ number;
  var_country_voisins2$ number;
  
  --CURSOR c
    BEGIN
    var_country_code$ := 'F';

    SELECT COUNT(Country2) INTO var_country_voisins1$
    FROM BORDERS
    WHERE var_country_code$ = BORDERS.Country1
    GROUP BY Country1;

    SELECT COUNT(Country1)INTO var_country_voisins2$
    FROM BORDERS
    WHERE var_country_code$ = BORDERS.Country2
    GROUP BY Country2;
    
  --SELECT Country.code, Country.name, SUM(NbVoisins) INTO var_country_code$, var_country_name$, var_country_voisins$
  --FROM Country JOIN tableVoisins ON (Country.code=tableVoisins.Code)
  --WHERE var_country_code$ = Country.code
 -- GROUP BY Country.code, Country.name ;

dbms_output.put_line(var_country_voisins1$ || ' blabla '||var_country_voisins2$);

END;
/
show ERRORS;

-- D�part qui pourra �tre fait avec un curseur

BEGIN
  With tableVoisins (Code, NbVoisins) AS (
    SELECT Country1 as Code, COUNT(Country2)
    FROM BORDERS
    GROUP BY Country1

    UNION 

    SELECT Country2 as Code, COUNT(Country1)
    FROM BORDERS
    GROUP BY Country2)
  SELECT Country.code, Country.name, SUM(NbVoisins) INTO var_country_code$, var_country_name$, var_country_voisins$
  FROM Country JOIN tableVoisins ON (Country.code=tableVoisins.Code)
  WHERE var_country_code$ = Country.code
  GROUP BY Country.code, Country.name ;

dbms_output.put_line('blabla'||var_country_voisins$);

END;
/
show ERRORS;


----- Autre solution 
DECLARE
  -- var_country_code$ varchar2(4);
  var_country_code$ Country.Code%type;
  var_country_name$ Country.name%type;
  var_country_name$ varchar;
  
BEGIN
  With tableVoisins (Code) AS (
    SELECT Country1
    FROM BORDERS
    UNION 
    SELECT Country2
    FROM BORDERS)
    
  SELECT Country.code, Country.name, Count(tableVoisins.code)
  FROM Country JOIN tableVoisins ON (Country.code=tableVoisins.Code)
  GROUP BY Country.code 
  
dbms_output.put_line('blabla'||)

END;
/


-------------------

-- code ok !
  With tableVoisins (Code, NbVoisins) AS (
    SELECT Country1 as Code, COUNT(Country2)
    FROM BORDERS
    GROUP BY Country1

    UNION 

    SELECT Country2 as Code, COUNT(Country1)
    FROM BORDERS
    GROUP BY Country2)
  SELECT Country.code, Country.name, SUM(NbVoisins)
  FROM Country JOIN tableVoisins ON (Country.code=tableVoisins.Code)
  GROUP BY Country.code, Country.name ;
  
  -- Non fonctionnel (petite erreur, renvoit que des 1)
  With tableVoisins (Code) AS (
    SELECT Country1
    FROM BORDERS
    UNION 
    SELECT Country2
    FROM BORDERS)
    
  SELECT Country.code, Country.name, Count(tableVoisins.code)
  FROM Country JOIN tableVoisins ON (Country.code=tableVoisins.Code)
  GROUP BY Country.code, Country.name ;