-- Requ�te SQL
--tous les pays par ordre alphabetique
SELECT Name
FROM Country
ORDER BY Name ASC; 

-- organisation avec le plus de pays
SELECT Organization, COUNT(country) as NbPays
FROM IS_MEMBER
GROUP BY Organization
HAVING COUNT(country) = (
  SELECT MAX(COUNT(country)) 
  FROM IS_MEMBER
  GROUP BY Organization
  )
;
-- ORDER BY COUNT(country) DESC; Pas utile car on r�cup�re que le(s) premier(s)

-- Fonctionne en une seule requ�te (voir suivante)
SELECT Country.Code as NomPays, SUM(City.population*100/Country.Population) as PourcentagePopVille
FROM CITY JOIN COUNTRY ON (City.Country = Country.code)
WHERE City.population IS NOT NULL
GROUP BY Country.Code
ORDER BY SUM(City.population*100/Country.Population) DESC;

-- requ�te interm�diaire pour r�cup�rer la population citadine.
With PopCitadine(Country, PopCita) AS ( 
SELECT Country, SUM(Population) as PopCita
FROM CITY
GROUP BY Country)
-- requ�te compl�te
SELECT Country.name, (PopCitadine.PopCita)*100/(Country.population)
FROM Country JOIN PopCitadine ON (Country.CODE = PopCitadine.Country)
WHERE PopCitadine.PopCita IS NOT NULL
ORDER BY (PopCitadine.PopCita)*100/(Country.population) DESC;

-- Population Des continents // Note : on divise par 100 car les pourcentages sont directement en pourcents dans la table
SELECT Encompasses.Continent as NomContinent , SUM(Encompasses.percentage * country.population/100) as PopulationContinentale
FROM Encompasses JOIN COUNTRY ON (Encompasses.Country = Country.code)
GROUP BY continent
ORDER BY SUM(Encompasses.percentage * country.population) DESC;

-- La plus grande fronti�re
With tableSomme (Code, Border) AS (
SELECT Country1 as Code, SUM(Length)
FROM BORDERS
GROUP BY Country1

UNION 

SELECT Country2 as Code, SUM(Length)
FROM BORDERS
GROUP BY Country2)

SELECT Country.name, SUM(tablesomme.Border)
FROM tableSomme JOIN country ON (tableSomme.Code = country.code)
GROUP BY COUNTRY.name
HAVING SUM(tablesomme.Border) = (
  SELECT MAX(SUM(tablesomme.Border)) 
  FROM tableSomme JOIN country ON (tableSomme.Code = country.code)
  GROUP BY COUNTRY.name
  )
;

-- Villes ayant plusieurs occurences dans pays diff�rents
SELECT Name, count(country) as NbOcurrences
FROM City
GROUP BY Name
HAVING count(Country) > 1 ;

-- Villes plusieurs pays + fioritures
SELECT Name, count(country) as NbOcurrences
FROM City
GROUP BY Name
ORDER BY count(country) DESC, Name ASC;

--Pas d'ind�pendance ! 
SELECT country.name, country.code
FROM Politics JOIN country ON (country.code = Politics.country)
WHERE independence IS NULL;

-- Pas d'organisation ! 
SELECT country.name, country.code, organization
FROM Is_member JOIN country ON (country.code = Is_member.country)
WHERE organization IS NULL;
-- Marche pas ci dessus, car il n'y as pas de null dans les tables ! On ne les cr�� par avec un join ! 

-- Pas d'organisation ! (qui marche!)
SELECT  country.name, country.code
FROM country
WHERE country.code NOT IN (
  SELECT Is_member.country 
  FROM Is_member);

-- Pays membre de ..
SELECT country.name, country.code, organization
FROM Is_member JOIN country ON (country.code = Is_member.country);

--
With tableSomme (Code, Border) AS (
SELECT Country1 as Code, SUM(Length)
FROM BORDERS
GROUP BY Country1

UNION 

SELECT Country2 as Code, SUM(Length)
FROM BORDERS
GROUP BY Country2)

SELECT continent, SUM(Border) -- Note, on peut faire un "percentage*Border" si on veut pond�rer
FROM encompasses JOIN tableSomme ON (encompasses.country = tablesomme.code)
GROUP BY continent;
