-- Supprimer les anciennes contraintes

ALTER TABLE City
DROP CONSTRAINT fk_City_Province;
ALTER TABLE City
DROP CONSTRAINT fk_City_Country;
ALTER TABLE Province
DROP CONSTRAINT fk_Province_Country;
ALTER TABLE Country
DROP CONSTRAINT u_Country_Name;

-- Cl�s etrangeres
-- Cl�s etrangeres City avec Province -- 
ALTER TABLE City
ADD CONSTRAINT fk_City_Province 
FOREIGN KEY (Province, Country)
REFERENCES Province (Name, Country)
ON DELETE SET NULL;
-- OK !

-- Cl�s etrangeres City avec Country -- 
ALTER TABLE City
ADD CONSTRAINT fk_City_Country 
FOREIGN KEY (Country)
REFERENCES Country (Code)
ON DELETE SET NULL;
-- ON DELETE CASCADE;
-- OK !

-- Cl�s etrangeres Province avec Country -- 
ALTER TABLE Province
ADD CONSTRAINT fk_Province_Country
FOREIGN KEY (Country)
REFERENCES Country(Code)
ON DELETE SET NULL;
-- OK !

-- Contraintes unicit�-- 
ALTER TABLE Country
ADD CONSTRAINT u_Country_Name
UNIQUE (Name);

-- Contraintes attributs-- 
-- Longueur sup � 0
ALTER TABLE Borders
ADD CONSTRAINT c_Borders_Length
CHECK (Length >0);

-- Nom pays non null
ALTER TABLE Country
ADD CONSTRAINT c_Country_Name
CHECK (Name IS NOT NULL);

-- Les % de 0 � 100
ALTER TABLE Encompasses
ADD CONSTRAINT c_Encompasses_Po
CHECK (Percentage>=0 AND Percentage<=100);

ALTER TABLE Economy
ADD CONSTRAINT c_Economy_Service_Po
CHECK (Service>=0 AND Service<=100);

ALTER TABLE Economy
ADD CONSTRAINT c_Economy_Industry_Po
CHECK (Industry>=0 AND Industry<=100);

ALTER TABLE Economy
ADD CONSTRAINT c_Economy_Agriculture_Po
CHECK (Agriculture>=0 AND Agriculture<=100);

--Creation des pays Atlan et Tis
ALTER TABLE Economy
ADD CONSTRAINT c_Economy_Agriculture_Po
CHECK (Agriculture>=0 AND Agriculture<=100);
-- NOTE : on a des check en double. Il faudra les supprimer ! 


-- Commentaire -- 

