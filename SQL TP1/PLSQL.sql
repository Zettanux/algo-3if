set serveroutput on;
/

-- Lister les voisins de pays qui ont une fronti�re commune
DECLARE
  
  -- D�claration d'un curseur qui parcours BORDERS
  CURSOR C_BORDERS IS
  SELECT country1, country2
  FROM BORDERS;
  
  -- Nos variables
  var_country_code$ Country.Code%type;
  var_neighboor_country_number$ number;
  var_country_name$ Country.Name%type;

  var_country_voisins1$ BORDERS.country1%TYPE;
  var_country_voisins2$ BORDERS.country2%TYPE;
  
  --CURSOR
    BEGIN
    var_country_code$ := 'AFG';
    var_neighboor_country_number$ := 0;
    
    Open C_BORDERS;
    Loop --boucle sur les lignes du curseur
      Fetch C_BORDERS INTO var_country_voisins1$,var_country_voisins2$;
      
      --Utilisation des donn�es
      IF var_country_voisins1$ = var_country_code$  THEN
        dbms_output.put_line(var_country_voisins1$ || ' voisin de  ' || var_country_voisins2$);
        var_neighboor_country_number$ := var_neighboor_country_number$ +1;
      ELSIF var_country_voisins2$ = var_country_code$ THEN 
        var_neighboor_country_number$ := var_neighboor_country_number$ +1;
        dbms_output.put_line(var_country_voisins2$ || ' voisin de  ' || var_country_voisins1$);
      END IF;
      
      EXIT When C_BORDERS%NOTFOUND; -- sortie quand on a plus de lignes
    End Loop;
    Close C_BORDERS;
   
    SELECT name INTO var_country_name$ 
    FROM COUNTRY
    WHERE Code = var_country_code$;
    
    dbms_output.put_line('Nom du pays  ' || var_country_name$);
    dbms_output.put_line('Nombre de voisins  ' || var_neighboor_country_number$);
   
   END;
/

-- Ajouter une colonne � Country
ALTER TABLE COUNTRY 
ADD frontiereLongue VARCHAR2(45);

-- Ajouter dans la colonne de Country, le pays qui a la plus grande fronti�re avec le pays en question du tuple
DECLARE
    -- D�claration d'un curseur qui parcours COUNTRY
  CURSOR C_COUNTRY IS
  SELECT CODE
  FROM COUNTRY;
  
  -- D�claration d'un curseur qui parcours BORDERS
  CURSOR C_BORDERS IS
  SELECT country1, country2, length
  FROM BORDERS;
  
    -- Nos variables
  var_country_code$ Country.Code%type;
  
  -- longueur de la frontiere courante et nom du pays associ� courant
  var_current_frontiere_length$ BORDERS.LENGTH%Type;
  -- Le nom des voisins courants
  var_country_voisins1$ BORDERS.country1%TYPE;
  var_country_voisins2$ BORDERS.country2%TYPE;
  
  -- longueur de la frontiere max et nom du pays associ� max
  var_max$ BORDERS.LENGTH%Type;
  var_country_code_voisins_max$ Country.Code%type;
  
  BEGIN
  
  Open C_COUNTRY;
  Loop --boucle sur les lignes du curseur
      Fetch C_COUNTRY INTO var_country_code$;
      var_max$ := 0;
      var_country_code_voisins_max$ := NULL;
      
      Open C_BORDERS;
      Loop --boucle sur les lignes du curseur
        Fetch C_BORDERS INTO var_country_voisins1$,var_country_voisins2$,var_current_frontiere_length$;
      
        IF var_country_voisins1$ = var_country_code$  THEN
        
          IF var_max$ < var_current_frontiere_length$ THEN
            var_max$ := var_current_frontiere_length$;
            var_country_code_voisins_max$ := var_country_voisins2$;
          END IF;
          
        ELSIF var_country_voisins2$ = var_country_code$ THEN 
        
          IF var_max$ < var_current_frontiere_length$ THEN
            var_max$ := var_current_frontiere_length$;
            var_country_code_voisins_max$ := var_country_voisins1$;
          END IF;
          
        END IF;
        
        EXIT When C_BORDERS%NOTFOUND; -- sortie quand on a plus de lignes
      End Loop;
      Close C_BORDERS;
      
      UPDATE Country
      SET frontiereLongue = var_country_code_voisins_max$
      WHERE code = var_country_code$;
      
      dbms_output.put_line('Nom du pays  ' || var_country_code$);
      dbms_output.put_line('Nom du voisin  ' || var_country_code_voisins_max$);
      dbms_output.put_line('Longueur frontiere  ' || var_max$);
      
    EXIT When C_COUNTRY%NOTFOUND; -- sortie quand on a plus de lignes
  End Loop;
  Close C_COUNTRY;
  END; 
/

-- Parce que c'est trop cool (afficher le nom des jours selon la date)
SELECT to_char(Independence, 'DAY')
FROM POLITICS;

-- Afficher les pays dont l'ind�pendence s'est d�roul�e un lundi
DECLARE
    -- D�claration d'un curseur qui parcours COUNTRY
  CURSOR C_POLITICS IS
  SELECT COUNTRY,INDEPENDENCE
  FROM POLITICS;
  
    -- Nos variables
  var_country_code$ Politics.country%type;
  var_independence_date Politics.independence%type;
  var_independence_date_day VARCHAR2(45);
  var_country_name$ Country.name%type;

  BEGIN
  
  Open C_POLITICS;
  Loop --boucle sur les lignes du curseur
      Fetch C_POLITICS INTO var_country_code$,var_independence_date;
      
      var_independence_date_day := to_char(var_independence_date, 'D');
      --dbms_output.put_line('Date  ' || var_independence_date_day);

      IF var_independence_date_day = 1 THEN
        SELECT name INTO var_country_name$
        FROM Country
        WHERE code = var_country_code$;
        
        dbms_output.put_line('Nom du pays  ' || var_country_name$);
      END IF;
      
    EXIT When C_POLITICS%NOTFOUND; -- sortie quand on a plus de lignes
  End Loop;
  Close C_POLITICS;
  END; 
/

--OU JUSTE FAIRE, car on pouvait aussi la faire en SQL
select 'Nom du pays  '||country.NAME
from politics join country on (code=country)
where to_char(INDEPENDENCE, 'D')=1
;

/**�crire du code qui permet d�ins�rer un pays dans la base. Cette proc�dure recevra comme param�tres
le nom du pays, son code, la capitale, la superficie, la population, le type du gouvernement et la date
d�ind�pendance. Traiter les exceptions.**/

DECLARE

  BEGIN 
    
  
  END; 
/

commit;
