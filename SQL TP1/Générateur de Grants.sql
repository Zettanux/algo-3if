--creation du fichier.sql

/*set pagesize 500 -- nb de ligne avant rappel des headers
set linesize 200 -- longueur par d�faut de la taille de la ligne
set heading off -- on enl�ve les headers
set echo off -- enl�ve le rapelle des commandes utilis�es
set feedback
*/
SET HEADING OFF FEEDBACK OFF ECHO OFF PAGESIZE 0 TRIMSPOOL ON  TRIMOUT OFF TERMOUT OFF VERIFY OFF FEED OFF;
SPOOL E:\INSA\2016-2017\SQL\GrantDroitToVFalconier.sql

--donne toutes les tables de l'utilisateur
SELECT 'grant select on ' || TABLE_NAME || ' to vfalconier;'
FROM user_tables;

SELECT 'grant update on ' || TABLE_NAME || ' to vfalconier;'
FROM user_tables;

SPOOL OFF 


-- @E:\INSA\2016-2017\SQL\GrantDroitToVFalconier.sql