#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;
//

/*
 *T'as un car qui contient une capacité max de N kg, puis un nombre nbe d'élève total, et pe le poids de chaque étudiant, ce car ne contient que des élèves de 3 eme année ou que des 4eme année (faire l'algo de sac)
Exemple d'entrée:
200
5
80 3
50 4
50 3
40 4
70 4
Sortie: soit tu renvois "3" soit "4" soit "3 4" 
 * */
 
bool sacADosUneOccurenceParObjet(int listeObjets[], int nbObjets, int capacite);
int sacADosUneOccurenceParObjetMAX(int listeObjets[], int nbObjets, int capacite);

int main()
{

    int capacite, nbObjets = 0, valCourante = 0, typeObjetCourant = 0, nbObjets4A = 0, nbObjets3A = 0;

    cin >> capacite;
    cin >> nbObjets;

    int listeObjets3A[nbObjets];
    int listeObjets4A[nbObjets];

    for (int i = 0; i < nbObjets; i++)
    {
        cin >> valCourante >> typeObjetCourant;
        if (typeObjetCourant == 3)
        {
            listeObjets3A[nbObjets3A] = valCourante;
            nbObjets3A++;
        }
        else if (typeObjetCourant == 4)
        {
            listeObjets4A[nbObjets4A] = valCourante;
            nbObjets4A++;
        }
        //DEBUG // cout << valCourant;
    }

    int ok3A = sacADosUneOccurenceParObjetMAX(listeObjets3A, nbObjets3A, capacite);
    int ok4A = sacADosUneOccurenceParObjetMAX(listeObjets4A, nbObjets4A, capacite);

    if (ok3A == ok4A && ok3A != 0)
    {
        cout << "3 4" << "\r\n";
    }
    else if (ok3A > ok4A)
    {
        cout << "3" << "\r\n";
    }
    else if (ok4A > ok3A)
    {
        cout << "4" << "\r\n";
    }
    else
    {
        cout << "NON" << "\r\n";
    }

    return 0;
}

bool sacADosUneOccurenceParObjet(int listeObjets[], int nbObjets, int capacite)
{
    bool answer = false;

    //Declaration du tableau de boolean avec capacite+1 case pour le zéro
    bool sac[capacite + 1];

    // Initialisation du sac : true pour 0 false pour le reste
    for (int j = 1; j <= capacite; j++)
    {
        sac[j] = false;
    }
    sac[0] = true;

    for (int i = 0; i < nbObjets; i++)
    { //pour chaque objet
        for (int j = capacite; j >= 0; j--)
        { //Pour chaque case du tableau de booléan
            if (sac[j] == true && listeObjets[i] + j <= capacite)
            { //Si on tombe sur une case accessible, et qu'on ne sort pas du tableau
                sac[listeObjets[i] + j] = true; // On pose un "true" pour dire que la case est accessible.
            }
            // DEBUG // cout << sac[j]; // Affichage de chaque "point" dans la ligne /!\ affichage inversé ! (le début est à droite !)
        }
        //DEBUG // cout << endl; // pour mieux lire l'affichage précédant
    }

    answer = sac[capacite]; //Si la dernière case est true, answer est true.
    return answer;
}

int sacADosUneOccurenceParObjetMAX(int listeObjets[], int nbObjets, int capacite)
{
    int answer = 0;

    //Declaration du tableau de boolean avec capacite+1 case pour le zéro
    bool sac[capacite + 1];

    // Initialisation du sac : true pour 0 false pour le reste
    for (int j = 1; j <= capacite; j++)
    {
        sac[j] = false;
    }
    sac[0] = true;

    for (int i = 0; i < nbObjets; i++)
    { //pour chaque objet
        for (int j = capacite; j >= 0; j--)
        { //Pour chaque case du tableau de booléan
            if (sac[j] == true && listeObjets[i] + j <= capacite)
            { //Si on tombe sur une case accessible, et qu'on ne sort pas du tableau
                sac[listeObjets[i] + j] = true; // On pose un "true" pour dire que la case est accessible.
            }
            // DEBUG // cout << sac[j]; // Affichage de chaque "point" dans la ligne /!\ affichage inversé ! (le début est à droite !)
        }
        //DEBUG // cout << endl; // pour mieux lire l'affichage précédant
    }

#ifdef MAP
    cout << " == CONTENU DU SAC == " << endl;
    for (int j = capacite; j >= 0; j--)
    {
        cout << sac[j];
    }
    cout << " == FIN CONTENU DU SAC == " << endl;

#endif
    for (int j = capacite; j >= 0; j--)
    {
        if (sac[j] == true)
        {
            answer = j;
            break;
        }
    }
    //Récupère la capacité maximale qu'on peut atteindre.
    return answer;
}
