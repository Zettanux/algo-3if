/*************************************************************************
                           TEnsemble  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation du module <591> (fichier 591.cpp) ---------------

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <string>
#include <sstream>
using namespace std;
//------------------------------------------------------ Include personnel
//////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées
//static type nom ( liste de paramètres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

static void test1()
{
	
}

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques

int main()
{	
	int nbColonnes = -1, nbSet =0;
	string answer = "";
	cin >> nbColonnes;

	while(nbColonnes != 0){	
		nbSet++;
			
		int tableau[nbColonnes];	
		int somme =0, hauteurFinale = 0, nbDeplacement = 0;
		
		for( int i=0 ; i < nbColonnes ; i++)
		{
			cin >> tableau[i];
			somme += tableau[i];
		}
		
		hauteurFinale = somme/nbColonnes;
		
		for( int i=0 ; i < nbColonnes ; i++)
		{
			if(tableau[i] > hauteurFinale)
			{
				nbDeplacement += tableau[i] - hauteurFinale;
			}
		}
		
		cin >> nbColonnes;
		cout << "Set #" << nbSet << "\n";
		cout << "The minimum number of moves is " <<  nbDeplacement << "." << "\n\n";
	}
	
	
	//test1(); 
/*
	istringstream iss( ligne ); 
    string mot; 
    while ( std::getline( iss, mot, ' ' ) ) 
    { 
        cout << mot << '\n'; 
    } 
	
	
	// ---------------------
string parsed,input="text to be parsed";
stringstream input_stringstream(input);

if(getline(input_stringstream,parsed,' '))
{
     // do some processing.
}


//==============
std::string s = "scott>=tiger>=mushroom";
std::string delimiter = ">=";

size_t pos = 0;
std::string token;
while ((pos = s.find(delimiter)) != std::string::npos) {
    token = s.substr(0, pos);
    std::cout << token << std::endl;
    s.erase(0, pos + delimiter.length());
}
std::cout << s << std::endl;
	*/
	
	return 0; 
	
} //----- fin de main

