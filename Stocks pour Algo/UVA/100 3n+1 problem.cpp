/*************************************************************************
                           TEnsemble  -  description
                             -------------------
    début                : $DATE$
    copyright            : (C) $YEAR$ par $AUTHOR$
    e-mail               : $EMAIL$
*************************************************************************/

//---------- Réalisation du module <100> (fichier 100.cpp) ---------------

/////////////////////////////////////////////////////////////////  INCLUDE
//-------------------------------------------------------- Include système
#include <iostream>
#include <string>
using namespace std;
//------------------------------------------------------ Include personnel
//////////////////////////////////////////////////////////////////  PRIVE
//------------------------------------------------------------- Constantes

//------------------------------------------------------------------ Types

//---------------------------------------------------- Variables statiques

//------------------------------------------------------ Fonctions privées
//static type nom ( liste de paramètres )
// Mode d'emploi :
//
// Contrat :
//
// Algorithme :
//
//{
//} //----- fin de nom

int calculFlyTime(unsigned int number)
{
	int result = 1;
	while(number!=1)
	{
		if(number%2 == 1)
		{
			number = 3*number+1;
		}
		else 
		{
			number = number/2;
		}
		result++;
	}
	
	return result;
}

int maxFlyTime(unsigned int numberA, unsigned int numberB)
{
	int max = 0, courant;
	
	for(int i=numberA; i <numberB+1 ; i++){
		courant = calculFlyTime(i);
		if(courant>max)
		{
			max = courant;
		}
	}
	
	return max;
}

//////////////////////////////////////////////////////////////////  PUBLIC
//---------------------------------------------------- Fonctions publiques

int main()
{	
	unsigned int a, b;
		
	while( scanf ("%d %d", &a, &b) != EOF ){
		if(a<b)
		{
					cout << a << " " << b << " " << maxFlyTime(a,b) << "\n";
		}
		else
		{
					cout << a << " " << b << " " << maxFlyTime(b,a) << "\n";
		}
	}
	
	//test1(); 
/*
	istringstream iss( ligne ); 
    string mot; 
    while ( std::getline( iss, mot, ' ' ) ) 
    { 
        cout << mot << '\n'; 
    } 
	
	
	// ---------------------
string parsed,input="text to be parsed";
stringstream input_stringstream(input);

if(getline(input_stringstream,parsed,' '))
{
     // do some processing.
}


//==============
std::string s = "scott>=tiger>=mushroom";
std::string delimiter = ">=";

size_t pos = 0;
std::string token;
while ((pos = s.find(delimiter)) != std::string::npos) {
    token = s.substr(0, pos);
    std::cout << token << std::endl;
    s.erase(0, pos + delimiter.length());
}
std::cout << s << std::endl;
	*/
	
	return 0; 
	
} //----- fin de main

