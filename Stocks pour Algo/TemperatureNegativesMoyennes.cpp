/* 
 * File:   main.cpp
 * Author: zettanux
 *
Problème numéro 1 : la température négative moyenne
---------------------------------------------------
Vous disposez de l'historique des températures à Lyon prises chaque heure. Vous devez calculer la température moyenne pour toutes les valeurs négatives de température (<= 0). Attention : 0 est considéré comme une valeur négative dans ce problème ! 
 
Description de l'entrée (IN) : la première ligne va contenir le nombre de valeurs en entrée et les lignes suivantes correspondent aux valeurs de température, une par ligne (valeurs réelles).
Description de la sortie (OUT) : un nombre représentant la moyenne des températures négatives. Si dans les données d'entrée il n'y a pas de température négative, affichez le caractère '-'. Observation : chaque ligne affichée sera terminée par deux caractères (CR et LF), correspondant à la chaine {"\r\n"}.

Exemple 1 
IN:
3
10
-5
1

OUT:
-5
 * Created on 26 novembre 2016, 11:17
 */

#include <iostream>
#include <stdio.h>
using namespace std;

int main()
{
    int nMeasures = 0, nbNegatives = 0;
    long double sum = 0;
    cin >> nMeasures;

    for (int nPos = 0; nPos < nMeasures; nPos++)
    {
        float fValue = 0;
        cin >> fValue;
        if (fValue <= 0)
        {
            nbNegatives++;
            sum += fValue;

            //cout.precision(10);
            //cout << "Valeur ajoutée : " << fValue << " pour " << sum << endl;
        }
    }

    if (nbNegatives == 0)
    {
        cout << "-" << "\r\n";

    }
    else
    {
        float fAverage = sum / nbNegatives;
        printf("%f", fAverage); // affiche 8 décimales
        cout << "\r\n";
        //cout.precision(150);
        //cout << fAverage << "\r\n";
    }

    return 0;
}
