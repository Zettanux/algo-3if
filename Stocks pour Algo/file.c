#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
  int indiceDebut; /* haut de la pile */
  int indiceLibre;    /* pointe sur la prochaine case libre */
  int *array; /* pointeur sur tableau*/
} Pile;

/* Init allocates the structure BinaryHeap and
 * also the membre array with the given size 
 * it also fill allocated (size) and intializes 
 * filled to 0 */
Pile * Init(int size);

/* InsertValue insert value into the binary heap
 * the array is reallocated if necessary (allocated changed 
 * with respect to the new size )
 * filled is incremented by 1 */
void InsertValue(Pile * pile, int value);

/* ExtractMAx returns 0 if the binary heap is empty
 * otherwise it return 1 and fills *val with the maximum 
 * value present in the binary heap
 * filled is decremented by 1  and the max value is removed
 * from the binary heap */
int Extract(Pile * pile, int * val);

/* Destroy frees the structure and the array */
void Destroy(Pile * pile);


int main(void) 
{
  char lecture[100];
  int val;
  Pile * pile;
  pile = Init(101); /**101 cases dans la pile dont 1 tjs vide**/

  fscanf(stdin,"%99s",lecture);
  while (strcmp(lecture,"bye")!=0) {
    if (strcmp(lecture,"queue")==0) {
      fscanf(stdin,"%99s",lecture);
      val = strtol(lecture,NULL,10); //transforme chaine de caracteres en un entier base 10
      InsertValue(pile,val);
    } else if (strcmp(lecture,"dequeue")==0) {
      if(Extract(pile,&val))
      {
        printf("%d\r\n",val);
      }
    }
    fscanf(stdin,"%99s",lecture);
  }
  Destroy(pile);
  return 0;
}

Pile * Init(int size)
{
  Pile * pile;
  pile = (Pile*) malloc(sizeof(Pile)); //Systeme cherche tout seul la taille qu'il faut.
  pile->array = (int*) malloc(size * sizeof(int)); // On alloue le tableau dans la structure.
  pile->indiceLibre = 0; 
  pile->indiceDebut = 0;
  
  return pile;
}

void InsertValue(Pile * pile, int value)
{
	if((pile->indiceLibre + 1)%101 != pile->indiceDebut)
	{
		pile->array[pile->indiceLibre] = value;
		pile->indiceLibre = (pile->indiceLibre + 1)%101; //taille tableau = 101
		//printf("VALEUR AJOUTEE");
	} else 
	{
		//printf("VALEUR NON AJOUTEE");
	}
		
  
}

int Extract(Pile * pile, int *res)
{
  if(pile->indiceLibre != pile->indiceDebut)
	{
		*res = pile->array[pile->indiceDebut];
		pile->indiceDebut = (pile->indiceDebut + 1)%101; //taille tableau = 101
		//printf("VALEUR RETIREE");
		return 1; //si return 1 alors on affiche valeur (dans main)
	} else 
	{
		return 0;
		//printf("VALEUR NON RETIREE");
	} 
}

void Destroy(Pile * pile)
{
  free(pile->array);
  pile->indiceDebut =0; 
  pile->indiceLibre =0;
  free(pile);
}

