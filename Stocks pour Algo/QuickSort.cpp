#include <stdio.h>

void quick_sort(int *a, int n)
{
    int i, j, p, t;
    if (n < 2)
    {
        return;
    }

    //Prend un valeur au moitié de la taille du tableau
    p = a[n / 2];

    //Un variable dans un sens, l'autre dans l'autre. On break quand besoin
    for (i = 0, j = n - 1;; i++, j--)
    {
        //On cale nos deux variables, i à gauche, j à droite.
        while (a[i] < p)
            i++;

        while (p < a[j])
            j--;

        // Si i est à gauche de j, c'est probablement, qu'on a trié tous les éléments.
        if (i >= j)
            break;

        //Echange de les valeurs i et j, si elles sont supérieurs l'une à l'autre.
        //On a donc une liste de valeurs, qui sont inférieurs au pivot à gauche
        t = a[i];
        a[i] = a[j];
        a[j] = t;
    }
    
    /* Maintenant, tous les éléments inférieurs au pivot sont avant ceux
   supérieurs au pivot. On a donc deux groupes de cases à trier. On utilise
   pour cela... la méthode quickSort elle-même ! */
    quick_sort(a, i);
    quick_sort(a + i, n - i);
}

int main(void)
{
    int a[] = {4, 65, 2, -31, 0, 99, 2, 83, 782, 1};
    int n = sizeof a / sizeof a[0];
    int i;

    for (i = 0; i < n; i++)
    {
        printf("%d%s", a[i], i == n - 1 ? "\n" : " ");

    }

    quick_sort(a, n);

    for (i = 0; i < n; i++)
    {
        printf("%d%s", a[i], i == n - 1 ? "\n" : " ");
    }
    return 0;
}