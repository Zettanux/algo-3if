
#include <iostream>
#include <stdio.h>
using namespace std;
int* deriverPolynome(int coefficients[], int nbCoefficients);
/*
 * 
 * Tu dois pour un polynôme d'entrée renvoyer le polynôme derivé k fois
Entrée: P(X)=3X^2 +10X +1
2(le degré)
1(coefficient du monôme de degré 0)
10(coefficient du monôme de degré 1)
3(coefficient du monôme de degré 2)
1(le nombre de fois que tu le dérives)
 * */
 
int main()
{
    // Réucpère le degré
    int degree;
    cin >> degree;
    
    //Récupère les coefficients
    int coefficients[degree + 1];
    for (int i = 0; i < degree + 1; i++)
    {
        cin >> coefficients[i];
    }

    //Récupère le nombre de dérivation.
    int nbDerivation;
    cin >> nbDerivation;

    // Nouveau tableau, dérivation
    int* derive = coefficients;

    // Dérivons, dérivons
    
    for (int i = 0; i < nbDerivation; i++)
    {
        derive = deriverPolynome(derive, (degree + 1));
#ifdef MAP
        cout << "Polynome dérivé une fois" << endl;
#endif
    }

    for (int i = 0; i < degree + 1 - nbDerivation; i++)
    {
#ifdef MAP
        cout << "Coefficient de degré  " << i << endl;
#endif
        cout << derive[i] << "\r\n";
    }

    return 0;
}

int* deriverPolynome(int* coefficients, int nbCoefficients)
{
    int* coefficientsFinaux = new int[nbCoefficients - 1];

    for (int i = 0; i < nbCoefficients - 1; i++)
    {
        coefficientsFinaux[i] = coefficients[i + 1] * (i + 1);
#ifdef MAP
        cout << "Coefficient de degré  " << i << " est " << coefficientsFinaux[i] << endl;
#endif
    }

    return coefficientsFinaux;
}
