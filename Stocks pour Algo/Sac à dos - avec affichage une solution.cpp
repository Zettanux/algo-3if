#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;
//Sac à dos - 2

int main()
{
    int capacite, nbObjets = 0, valCourante = 0;
    int listeObjets[100];
    cin >> capacite;

    while (valCourante != -1)
    {
        cin >> valCourante;
        if (valCourante != -1)
        {
            listeObjets[nbObjets] = valCourante;
            nbObjets++;
        }
        //DEBUG // cout << valCourant;
    }

    //Declaration du tableau de boolean avec capacite+1 case pour le zéro
    bool sac[capacite + 1][nbObjets];

    // Initialisation du sac : true pour 0 false pour le reste
    for (int j = 1; j <= capacite; j++)
    {
        sac[j][0] = false;
    }
    sac[0][0] = true;

    for (int i = 0; i < nbObjets; i++)
    { //pour chaque objet
        for (int j = capacite; j >= 0; j--)
        { //Pour chaque case du tableau de booléan
            if (sac[j][0] == true && listeObjets[i] + j <= capacite)
            { //Si on tombe sur une case accessible, et qu'on ne sort pas du tableau
                sac[listeObjets[i] + j][0] = true; // On pose un "true" pour dire que la case est accessible.
                //On propage les objets qui nous on servi à atteindre la case d'avant.
                for (int k = 1; k < nbObjets; k++)
                {
                    sac[listeObjets[i] + j][k] = sac[j][k];
                }
            }
            // DEBUG // cout << sac[j]; // Affichage de chaque "point" dans la ligne /!\ affichage inversé ! (le début est à droite !)
        }
        //DEBUG // cout << endl; // pour mieux lire l'affichage précédant
    }

    for (int k = 1; k < nbObjets; k++)
    {
        if (sac[capacite][k] == true)
        {
            cout << " objet N°" << k << " pour poids : " << listeObjets[k-1] << endl;
        }
    }

    if (sac[capacite][0] == true)
    {
        cout << "OUI" << "\r\n";
    }
    else
    {
        cout << "NON" << "\r\n";
    }

    return 0;
}
