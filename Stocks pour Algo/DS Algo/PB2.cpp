#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

bool sacADosUneOccurenceParObjet(int listeObjets[], int nbObjets, int capacite);
int sacADosUneOccurenceParObjetMAX(int listeObjets[], int nbObjets,
		int capacite);

int main() {

	int capacite, nbObjets = 0, valCourante = 0, typeObjetCourant = 0,
			nbObjetsTous = 0, nbObjets2T = 0, nbObjets1T = 0;

	cin >> capacite;
	cin >> nbObjets;

	int listeObjetsT1[nbObjets];
	int listeObjetsT2[nbObjets];
	int listeObjetsTous[nbObjets];

	for (int i = 0; i < nbObjets; i++) {
		cin >> valCourante >> typeObjetCourant;
		if (typeObjetCourant == 1) {
			listeObjetsT1[nbObjets1T] = valCourante;
			nbObjets1T++;
		} else if (typeObjetCourant == 2) {
			listeObjetsT2[nbObjets2T] = valCourante;
			nbObjets2T++;
		}

		listeObjetsTous[nbObjetsTous] = valCourante;
		nbObjetsTous++;
#ifdef MAP
		cout << "dans tableau  : " << valCourante << endl;
#endif
		//DEBUG // cout << valCourant;
	}
#ifdef MAP
	cout << "Nomre objets finaux  : " << nbObjetsTous << endl;
#endif
	//int ok1T = sacADosUneOccurenceParObjetMAX(listeObjetsT1, nbObjets1T, capacite);
	//int ok2T = sacADosUneOccurenceParObjetMAX(listeObjetsT2, nbObjets2T, capacite);

	//int valeurFinale = sacADosUneOccurenceParObjetMAX(listeObjetsTous, nbObjetsTous, capacite);

	// PREMIERE LISTE
	//Declaration du tableau de boolean avec capacite+1 case pour le zéro
	bool sac1T[capacite + 1];

	// Initialisation du sac : true pour 0 false pour le reste
	for (int j = 1; j <= capacite; j++) {
		sac1T[j] = false;
	}
	sac1T[0] = true;

	for (int i = 0; i < nbObjets1T; i++) { //pour chaque objet
		for (int j = capacite; j >= 0; j--) { //Pour chaque case du tableau de booléan
			if (sac1T[j] == true && listeObjetsT1[i] + j <= capacite) { //Si on tombe sur une case accessible, et qu'on ne sort pas du tableau
				sac1T[listeObjetsT1[i] + j] = true; // On pose un "true" pour dire que la case est accessible.
			}
			// DEBUG // cout << sac[j]; // Affichage de chaque "point" dans la ligne /!\ affichage inversé ! (le début est à droite !)
		}
		//DEBUG // cout << endl; // pour mieux lire l'affichage précédant
	}

#ifdef MAP
	cout << " == CONTENU DU SAC 1 == " << endl;
	for (int j = capacite; j >= 0; j--)
	{
		cout << sac1T[j];
	}
	cout << " == FIN CONTENU DU SAC  1 == " << endl;
#endif

	//SECONDE LISTE
	//Declaration du tableau de boolean avec capacite+1 case pour le zéro
	bool sac2T[capacite + 1];

	// Initialisation du sac : true pour 0 false pour le reste
	for (int j = 1; j <= capacite; j++) {
		sac2T[j] = false;
	}
	sac2T[0] = true;

	for (int i = 0; i < nbObjets2T; i++) { //pour chaque objet
		for (int j = capacite; j >= 0; j--) { //Pour chaque case du tableau de booléan
			if (sac2T[j] == true && listeObjetsT2[i] + j <= capacite) { //Si on tombe sur une case accessible, et qu'on ne sort pas du tableau
				sac2T[listeObjetsT2[i] + j] = true; // On pose un "true" pour dire que la case est accessible.
			}
			// DEBUG // cout << sac[j]; // Affichage de chaque "point" dans la ligne /!\ affichage inversé ! (le début est à droite !)
		}
		//DEBUG // cout << endl; // pour mieux lire l'affichage précédant
	}

#ifdef MAP
	cout << " == CONTENU DU SAC 2 == " << endl;
	for (int j = capacite; j >= 0; j--)
	{
		cout << sac2T[j];
	}
	cout << " == FIN CONTENU DU SAC  2 == " << endl;
#endif

	// On se retrouve avec deux liste de boolean.
	int max = 0;
	int maxRougeCourant = 0;
	bool aFini = false;

	bool crible[capacite + 1];

	// Initialisation du sac : true pour 0 false pour le reste
	for (int j = 1; j <= capacite; j++) {
		crible[j] = false;
	}

	// Grosse boucle
	//while (aFini == false) {
	for (int j = capacite; j >= 0; j--) { //Pour chaque case du tableau de booléan

		// Si on a un valeur accessible dans la table 2
		if (sac2T[j] == true) {

			// On refait un tour, sans considérer la table 2, à la rechercher d'un table 1 qui convient.
			for (int k = capacite; k >= 0; k--) {
				int diff;
				if(j-k >= 0) {
					 diff = j-k;
				} else {
					diff = k-j;
				}
				if (sac1T[k] == true && (diff + 2*k) <= capacite) {
#ifdef MAP
	cout << "Si on prend la case : " << k << " ça donnerai" << (j-k + 2*k) << endl;
#endif
					if (max < 2*k) {
						max = 2*k;
#ifdef MAP
	cout << "MAX TROUVE pour quantité Rouge  : " << 2*k << endl;
#endif
					} else {
#ifdef MAP
	cout << "Incorrect" << endl;
#endif
					}
				}
			}

		// Si on a un valeur accessible dans la table 1
		} else if (sac1T[j] == true) {

			// On refait un tour, sans considérer la table 1, à la rechercher d'un table 2 qui convient.
			for (int k = capacite; k >= 0; k--) {
				//Si on a une case accessible dans 2, au rang "k", on voit ce que ça donnera : 2*rang k + ce qui restera
				int diff;
				if(j-k >= 0) {
					 diff = j-k;
				} else {
					diff = k-j;
				}
				if (sac2T[k] == true && (diff + 2*k) <= capacite) {
#ifdef MAP
	cout << "Si on prend la case : " << k << " ça donnerai" << (j-k + 2*k) << endl;
#endif
					// Si ça tient, super. on stocke.
					if (max < 2*k) {
						max = 2*k;
#ifdef MAP
	cout << "MAX TROUVE pour quantité Rouge : " << 2*k << endl;
#endif
					} else {
#ifdef MAP
	cout << "Incorrect" << endl;
#endif
					}
				}
			}

		}

	}


	  printf("%d\r\n",max);
	//cout << max << endl;

	return 0;
}

bool sacADosUneOccurenceParObjet(int listeObjets[], int nbObjets,
		int capacite) {
	bool answer = false;

	// Declaration du tableau de boolean avec capacite+1 case pour le zéro
	bool sac[capacite + 1];

	// Initialisation du sac : true pour 0 false pour le reste
	for (int j = 1; j <= capacite; j++) {
		sac[j] = false;
	}
	sac[0] = true;

	for (int i = 0; i < nbObjets; i++) { //pour chaque objet
		for (int j = capacite; j >= 0; j--) { //Pour chaque case du tableau de booléan
			if (sac[j] == true && listeObjets[i] + j <= capacite) { //Si on tombe sur une case accessible, et qu'on ne sort pas du tableau
				sac[listeObjets[i] + j] = true; // On pose un "true" pour dire que la case est accessible.
			}
			// DEBUG // cout << sac[j]; // Affichage de chaque "point" dans la ligne /!\ affichage inversé ! (le début est à droite !)
		}
//DEBUG // cout << endl; // pour mieux lire l'affichage précédant
	}

	answer = sac[capacite]; //Si la dernière case est true, answer est true.
	return answer;
}

int sacADosUneOccurenceParObjetMAX(int listeObjets[], int nbObjets,
		int capacite) {
	int answer = 0;

	//Declaration du tableau de boolean avec capacite+1 case pour le zéro
	bool sac[capacite + 1];

	// Initialisation du sac : true pour 0 false pour le reste
	for (int j = 1; j <= capacite; j++) {
		sac[j] = false;
	}
	sac[0] = true;

	for (int i = 0; i < nbObjets; i++) { //pour chaque objet
		for (int j = capacite; j >= 0; j--) { //Pour chaque case du tableau de booléan
			if (sac[j] == true && listeObjets[i] + j <= capacite) { //Si on tombe sur une case accessible, et qu'on ne sort pas du tableau
				sac[listeObjets[i] + j] = true; // On pose un "true" pour dire que la case est accessible.
			}
			// DEBUG // cout << sac[j]; // Affichage de chaque "point" dans la ligne /!\ affichage inversé ! (le début est à droite !)
		}
//DEBUG // cout << endl; // pour mieux lire l'affichage précédant
	}

#ifdef MAP
	cout << " == CONTENU DU SAC == " << endl;
	for (int j = capacite; j >= 0; j--)
	{
		cout << sac[j];
	}
	cout << " == FIN CONTENU DU SAC == " << endl;

#endif
	for (int j = capacite; j >= 0; j--) {
		if (sac[j] == true) {
			answer = j;
			break;
		}
	}
	//Récupère la capacité maximale qu'on peut atteindre.
	return answer;
}

