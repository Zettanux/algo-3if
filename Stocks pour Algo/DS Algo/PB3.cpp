#include <iostream>
#include <stdio.h>
using namespace std;

int main() {
	// Réucpère le degré
	int degree;
	cin >> degree;

	//Récupère les coefficients
	int coefficients[degree + 1];
	int evaluationParX[degree + 1];
	for (int i = 0; i < degree + 1; i++) {
		cin >> coefficients[i];
		evaluationParX[i] = coefficients[i];
	}

	//Récupère le nombre de dérivation.
	int modulo;
	cin >> modulo;

	//Récupère le x
	int evaluationX;
	cin >> evaluationX;

	// Nouveau tableau, dérivation
	int* derive = coefficients;

	double sum = 0;
	// Calculons
	for (int i = 0; i < degree+1; i++) {
		for (int j = 0; j < i; j++) {
			evaluationParX[i] *= evaluationX;
#ifdef MAP
		cout << "case "<< i << " contient " << evaluationParX[i] << endl;
#endif
		}
		sum+= evaluationParX[i];
#ifdef MAP
		cout << " somme "<< sum << endl;
#endif
	}

	int answer = ((int)sum )%modulo;
	printf("%d\r\n",answer);

	return 0;
}
