DS Algorithmique - 3IF - 08 janvier 2015
----------------------------------------

Contraintes
-----------
Les solutions au DS sont valid�es via la plateforme domjudge. Chaque �l�ve a 
son propre login et son propre mot de passe. Vous pouvez envoyer plusieurs 
fois une solution pour un m�me probl�me sans aucune p�nalit�.

Vous avez le droit aux supports des cours, TD, TP et aussi � des ressources 
Internet, mais vous n'avez pas le droit de communiquer avec d'autres personnes. 
La DSI sauvegarde tout le trafic TCP/IP pendant le DS. Vous pouvez utiliser 
seulement le protocole http pour acc�der aux diff�rents sites Web. Une 
connexion � un outil de messagerie, r�seau social etc invalide automatiquement
votre participation au DS avec les cons�quences pr�vues par le r�glement des 
�tudes.

Vous pouvez utiliser le langage C ou le C++, mais sans utiliser la STL.


Probl�me num�ro 3 : File impl�ment�e par une liste cha�n�e 
----------------------------------------------------------

* Introduction

Pour rappel, une file est un type de donn�es abstrait qui repr�sente une 
collection g�r�e en FIFO et offrant l'interface suivante :
  - EnFile permet d'ajouter un �l�ment
  - DeFile permet de supprimer un �l�ment et de le renvoyer
  - EstVide renvoie vrai si la file est vide

* Code de base

Le code de base qui est propos� ci-dessous et que vous devez utiliser permet
de g�rer toutes les entr�es-sorties et vous donne d�j� les structures de 
donn�es, vous n'avez que trois fonctions � impl�menter, dont le prototype est
donn� en commentaire.


/* probl�me numero 3 - file implement�e par une liste chain�e */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct Element {
   struct Element * suivant;
   int valeur;
} Element;

typedef struct {
  Element * debut; /* le d�but de la liste cha�n�e */
  Element * fin;   /* le dernier �l�ment de la liste */
} File;

void EnFile(File * file, int valeur);
int DeFile(File * file);
int EstVide(File file);
void Init(File * file);
void Destroy(File file);

void error(void);

int main(void) 
{
   int val;
   char lecture[100];
   File file;

   Init(&file);

   if (fscanf(stdin,"%99s",lecture)!=1)
      error();
   while (strcmp(lecture,"bye")!=0)
   {
      if (strcmp(lecture,"enfile")==0)
      {
         if (fscanf(stdin,"%99s",lecture)!=1)
            error();
         val = atoi(lecture);
         EnFile(&file,val);
      }
      else if (strcmp(lecture,"defile")==0)
      {
         val = DeFile(&file);
         printf("%d\r\n",val);
      }
      else if (strcmp(lecture,"estvide")==0)
      {
         printf("%s\r\n",EstVide(file)?"oui":"non");
      }

      if (fscanf(stdin,"%99s",lecture)!=1)
         error();
   }
   Destroy(file);
   return 0;
}

void EnFile(File * file, int valeur)
/* cette proc�dure enfile valeur dans la file */
/*  - file est un pointeur sur la structure de donn�es File et
 *    repr�sente une file bien form�e 
 *  - valeur est la valeur � ins�rer */
{
   /* ins�rer le code ici */
}

int DeFile(File * file)
/* cette fonction d�file la valeur et la renvoie */
/*  - file est un pointeur sur la structure de donn�es File et
 *    repr�sente une file bien form�e 
 *  - si la file est vide la valeur 0 est renvoy�e */  
{
   /* ins�rer le code ici */
}

int EstVide(File file)
/* cette fonction indique si la file est vide */
/*  - file est de type File et repr�sente une file bien form�e
 *  - la valeur renvoy�e vaut 1 si la file est vide, 0 sinon */
{
   /* ins�rer le code ici */
}

void Init(File * file)
{
   file->debut = NULL; 
   file->fin = NULL;
}

void Destroy(File file)
{
   Element * courant = file.debut;
   Element * suivant;
   while (courant != NULL)
   {
      suivant = courant->suivant;
      free(courant);
      courant = suivant;
   }
}

void error(void)
{
   printf("input error\r\n");
   exit(0);
}
