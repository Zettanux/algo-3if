#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//equations
int main(void) 
{
  double a, b, c;
  double delta;
  
  scanf("%lf", &a);
  scanf("%lf", &b);
  scanf("%lf", &c);
  
  delta = b*b - 4.0*a*c ;
  double x0 = (-b)/(2.0*a);
  double x1 = (-b-sqrt(delta))/(2.0*a);
  double x2 = (-b+sqrt(delta))/(2.0*a);

  if(delta == 0){
	  printf("1\n");
	  printf("%f\n",x0);
  } else if(delta < 0){
	  printf("0\n");
  } else if(delta > 0){
	  printf("2\n");
	  if(x1<x2){
		printf("%f\n",x1);
		printf("%f\n",x2);
	  } else {
		printf("%f\n",x2);
		printf("%f\n",x1);
	  }
  }
  
  return 0;
}
