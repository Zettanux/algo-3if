
#include <iostream>
#include <stdio.h>
using namespace std;
double* integrerPolynome(double coefficients[], int nbCoefficients);

/*
 * 
 * Tu dois pour un polynôme d'entrée renvoyer le polynôme intégré k fois
Entrée: P(X)=3X^2 +10X +1
2(le degré)
1(coefficient du monôme de degré 0)
10(coefficient du monôme de degré 1)
3(coefficient du monôme de degré 2)
1(le nombre de fois que tu le dérives)
 * */

int main()
{
    // Réucpère le degré
    int degree;
    cin >> degree;

    //Récupère les coefficients
    double coefficients[degree + 1];
    for (int i = 0; i < degree + 1; i++)
    {
        cin >> coefficients[i];
    }

    //Récupère le nombre d'intégrations.
    int nbIntegration;
    cin >> nbIntegration;

    // Nouveau tableau, dérivation
    double* integre = coefficients;

    // Intégrons, intrégrons

    for (int i = 0; i < nbIntegration; i++)
    {
        integre = integrerPolynome(integre, (degree + 1));
#ifdef MAP
        cout << "Polynome dérivé une fois" << endl;
#endif
    }

    for (int i = 0; i < degree + 1 + nbIntegration; i++)
    {
#ifdef MAP
        cout << "Coefficient de degré  " << i << endl;
#endif
        cout << integre[i] << "\r\n";
    }

    return 0;
}

double* integrerPolynome(double* coefficients, int nbCoefficients)
{
    double* coefficientsFinaux = new double[nbCoefficients + 1];

    for (int i = 0; i < nbCoefficients + 1; i++)
    {
        if (i == 0)
        {
            coefficientsFinaux[i] = 1;
        }
        else
        {
            coefficientsFinaux[i] = coefficients[i-1] / (i);
        }
#ifdef MAP
        cout << "Coefficient de degré  " << i << " est " << coefficientsFinaux[i] << endl;
#endif
    }

    return coefficientsFinaux;
}
