#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;
//Sac à dos - 1
int main() {
	int capacite, nbObjets = 0 ,valCourante = 0;
	int listeObjets[100];
	cin >> capacite;
	
	while(valCourante != -1){
		cin >> valCourante;
		if(valCourante != -1){
			listeObjets[nbObjets] = valCourante ;
			nbObjets++;
		}
		//DEBUG // cout << valCourant;
	}

	//Declaration du tableau de boolean avec capacite+1 case pour le zéro
	bool sac[capacite+1];
	// Initialisation du sac : true pour 0 false pour le reste
	for(int j = 1 ; j <= capacite ; j++){
		sac[j] = false;
	}
	sac[0] = true;
	
	for(int j = 0 ; j <= capacite ; j++){ //Pour chaque case du tableau de booléan
		for(int i= 0; i<nbObjets ; i ++){ //pour chaque objet
			if( j-listeObjets[i] >= 0 &&  listeObjets[i]<capacite){ //Si le capacité à remplir - taille de l'objet n'est pas en dehors du tableau et que l'objet n'est pas plus gros que la capacité totale
				if( sac[j-listeObjets[i]] == true ) { // Si on tombe sur un case qui a déjà été accessible par un tas d'objet
					sac[j] = true; // la case est accessible
				}
				
			}
		}
	}
	
	if(sac[capacite] == true){
		cout << "OUI" << "\r\n";
	} else {
		cout << "NON" << "\r\n";
	}

	return 0;
}
