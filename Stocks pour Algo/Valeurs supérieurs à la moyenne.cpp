
#include <iostream>
#include <stdio.h>
using namespace std;
/*
 * Entrée
-n:nombre de valeur
-val Les valeurs
Trouve le nombre de valeurs positives supérieurs à La moyenne(doit être un double pour avoir que des answers-correct)
Exemple d'entrée;
3
1
-2
5
 * */
int main()
{
    int nMeasures = 0, nbSupMoyenne = 0;
    double moy = 0, sum = 0;
    cin >> nMeasures;
    double valeurs[nMeasures];

    for (int i = 0; i < nMeasures; i++)
    {
        cin >> valeurs[i];
        // On ajoute à la somme
        sum += valeurs[i];
        //cout.precision(10);
        //cout << "Valeur ajoutée : " << fValue << " pour " << sum << endl;
    }

    //Calcul moyenne
    moy = sum / nMeasures;

#ifdef MAP
    cout << "Moyenne " << moy << endl;
#endif
    //On fait un passage pour voir si les valeur sont supérieur à cette moyenne
    for (int i = 0; i < nMeasures; i++)
    {
        if (valeurs[i] > moy)
        {
            nbSupMoyenne++;
#ifdef MAP
            cout << "Supérieur à la moyenne est la valeur:  " << valeurs[i] << endl;
#endif
        }
    }

    printf("%d", nbSupMoyenne); // affiche 8 décimales
    cout << "\r\n";
    //cout.precision(150);
    //cout << fAverage << "\r\n";


    return 0;
}
