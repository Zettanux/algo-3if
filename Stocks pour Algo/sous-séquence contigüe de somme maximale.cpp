/* 
 * File:   main.cpp
 * Author: zettanux
 *
Problème 3 : sous-séquence contigüe de somme maximale
-----------------------------------------------------

Pour une séquence de n nombres réels (0 < n <= 5000), nous vous demandons de trouver
la somme maximale d'une sous-séquence contigüe. 
Exemple 1 : pour la séquence 1 -2 10 -14 2 5 7 -1 le résultat 
est 14 (correspondant à la sous-séquence 2 5 7).

Exemple 2 : pour la séquence 1 -2 10 -2 2 5 7 -1 le résultat 
est 22 (correspondant à la sous-séquence 10 -2 2 5 7).

Observation : pour ce problème vous pouvez obtenir une erreur de type TIMELIMIT,
correspondant à une durée d'exécution trop importante de votre algorithme !
 * Created on 26 novembre 2016, 11:17
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main()
{
    int nbValeurs = 21;
    cin >> nbValeurs;

    double listeValeurs[nbValeurs];
    double sommeCumules[nbValeurs];
    double sommeCumulesPositives[nbValeurs];

    //Récupération des valeurs
    for (int i = 0; i < nbValeurs; i++)
    {
        cin >> listeValeurs[i];

        if (i == 0)
        {
            sommeCumules[i] = listeValeurs[i];
            //On ne prend que les positifs.
            if (listeValeurs[i] > 0)
            {
                sommeCumulesPositives[i] = listeValeurs[i];
            }
            else
            {
                sommeCumulesPositives[i] = 0;
            }
        }
        else
        {
            sommeCumules[i] = listeValeurs[i] + sommeCumules[i - 1];
            if (listeValeurs[i] + sommeCumulesPositives[i - 1] > 0)
            {
                sommeCumulesPositives[i] = listeValeurs[i] + sommeCumulesPositives[i - 1];
            }
            else
            {
                sommeCumulesPositives[i] = 0;
            }
        }
#ifdef MAP
        cout << "Case de liste N° : " << i << " de valeur " << listeValeurs[i] << endl;
        cout << "Case de somme N° : " << i << " de valeur " << sommeCumules[i] << endl;
        cout << "Case de somme positive N° : " << i << " de valeur " << sommeCumulesPositives[i] << endl;
#endif
    }

    double max = 0;

    for (int i = 0; i < nbValeurs; i++)
    {
#ifdef MAP
        cout << "Case de somme positive N° : " << i << " de valeur " << sommeCumulesPositives[i] << endl;
#endif
        //On trouve le maximum
        if (sommeCumulesPositives[i] > max)
        {
            max = sommeCumulesPositives[i];
#ifdef MAP
            cout << "Nouveau max : " << max << endl;
#endif
        }
    }


    //cout << " Le max : " << max << " de : " << indiceDebut << " à " << indiceFin;
    printf("%f\r\n", max);

    return 0;
}

/*

    double maxCourant = 0;
    double max = 0;
    int indiceDebutCourant = 0;
    int indiceDebut = 0;
    int indiceFin = 0;

    //Récupération du tableau
    for (int i = 0; i < nbValeurs; i++)
    {
        cin >> listeValeurs[i];
        if (i == 0)
        {
            sommeCumules[0] = listeValeurs[0];
        }
        else
        {
            sommeCumules[i] = listeValeurs[i] + sommeCumules[i - 1];
        }
#ifdef MAP
        cout << "Case sommeCumules N° : " << i << " de valeur " << sommeCumules[i] << endl;
#endif
        //Si on vient de tomber sur une chute OU qu'on est à la fin
        if ((i != 0 && sommeCumules[i] < sommeCumules[i - 1] && sommeCumules[i] < 0) )
        {
#ifdef MAP
            cout << "Chute de : " << sommeCumules[i-1] << " à " << sommeCumules[i] << endl;
            cout << "Calcul de la somme du N° " << indiceDebutCourant << " à " << i << endl;
#endif 
            //On calcul la somme précédente, donc de indiceDebutCourant à i-1 
            for (int j = indiceDebutCourant; j < i; j++)
            {
                maxCourant += listeValeurs[j];
            }
            //Si ce max est supérieur au max global, on stocke tout.
            if (maxCourant > max)
            {
                max = maxCourant;
                indiceDebut = indiceDebutCourant;
                indiceFin = i; //(excluant le i)
            }
            // On remet à 0
            maxCourant = 0;
            indiceDebutCourant = i+1;
        } else if((i == nbValeurs - 1)){
            #ifdef MAP
            cout << "Chute de : " << sommeCumules[i-1] << " à " << sommeCumules[i] << endl;
            cout << "Calcul de la somme du N° " << indiceDebutCourant << " à " << i << endl;
#endif 
            // On gère le dernier cas suivant si il augmente la somme ou non.
            int val = 0;
            if(listeValeurs[i]<0){
                val =i;
            } else {
                val =i+1;
            }
            
            for (int j = indiceDebutCourant; j < val; j++)
            {
                maxCourant += listeValeurs[j];
            }
            if (maxCourant > max)
            {
                max = maxCourant;
                indiceDebut = indiceDebutCourant;
                indiceFin = i; //(excluant le i)
            }
            // On remet à 0
            maxCourant = 0;
            indiceDebutCourant = i+1;
        }
        
        if(listeValeurs[i] > max)
        {
            max = listeValeurs[i];
        }
    } 
 */


/*
 
  //Récupération du tableau
    for (int i = 0; i < nbValeurs; i++)
    {
        cin >> listeValeurs[i];
        if (i == 0)
        {
            sommeCumules[0] = listeValeurs[0];
        }
        else
        {
            sommeCumules[i] = listeValeurs[i] + sommeCumules[i - 1];
        }

        if (maxCourant + listeValeurs[i] >= 0)
        {
            maxCourant = maxCourant + listeValeurs[i];
        }
        else
        {
            maxCourant = listeValeurs[i];
            indiceDebutCourant = i;
        }

        if (max < maxCourant)
        {
            max = maxCourant;
            indiceDebut = indiceDebutCourant;
            indiceFin = i;
        }
    }
 */
