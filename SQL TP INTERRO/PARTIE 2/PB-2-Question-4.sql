-- PB2 QUESTION 4
-- Vincent Falconieri; La�titia Lachat

--Question 2.4. Pour chaque langue, donnez la population totale qui l�utilise (tous pays confondus).
--La r�ponse devra faire appara�tre deux attributs : le nom de la langue ainsi que la population totale en nombre de personnes
--(approximativement).
--Le r�sultat sera affich� par ordre d�croissant de la population concern�e.

SELECT language.name, SUM(country.population) as nbPersonneQuiParlent
FROM Country JOIN language ON  (Country.code = language.country)
GROUP BY language.name
ORDER BY nbPersonneQuiParlent DESC;