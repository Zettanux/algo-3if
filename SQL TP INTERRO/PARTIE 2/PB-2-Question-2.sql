-- PB2 QUESTION 2
-- Vincent Falconieri; La�titia Lachat

--Question 2.2. Pour chaque langue pr�sente dans la base, afficher le nombre de pays dans lesquelles elles sont majoritaires.
--Ici, nous consid�rerons qu�une langue est majoritaire lorsqu�elle est parl�e par au moins 50% de la population.
--La r�ponse doit faire appara�tre deux attributs correspondant au nom de la langue et le nombre de pays dans lesquels
--elle est majoritaire.
--Les r�sultats doivent �tre class�s par nombre de pays (d�croissant), et en cas d��galit�, par ordre alphab�tique de la
--langue.
SELECT NAME, COUNT(Country) as nbPaysParle
FROM LANGUAGE
WHERE PERCENTAGE > 50
GROUP BY NAME
ORDER BY nbPaysParle DESC;
