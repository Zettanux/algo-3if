-- PB2 QUESTION 3
-- Vincent Falconieri; La�titia Lachat

--Question 2.3. Quelles sont les langues parl�es majoritaires du point de vue du nombre de pays (i.e. langues parl�es dans
--le plus grand nombre de pays), et combien de pays est-ce que cela repr�sente ?
--La r�ponse devra faire appara�tre deux attributs : le nom de la langue et le nombre de pays. Elle sera class�e par ordre
--alphab�tique des LANGUE.

SELECT NAME, COUNT(Country) as nbPaysParle
FROM LANGUAGE
GROUP BY NAME
ORDER BY nbPaysParle DESC, NAME ASC;
