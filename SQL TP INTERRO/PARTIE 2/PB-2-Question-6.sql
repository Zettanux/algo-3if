-- PB2 QUESTION 6
-- Vincent Falconieri; La�titia Lachat

--Question 2.6. Pour toutes les langues parl�es aux Etats-Unis, pourcentage de leur utilisation au Mexique.
--ATTENTION, Important : une ligne par langue imp�rativement, m�me si elles ne sont pas officiellement parl�es au
--Mexique !
--Les attributs pr�sents dans la r�ponse devront-�tre : Nom de la langue, pourcentage de la population mexicaine parlant
--cette langue.
--Le r�sultat doit �tre tri� par ordre alphab�tique des langues.

-- Normalement fonctionne. (NOTRE VRAI REPONSE)
SELECT L1.name, L2.PERCENTAGE
FROM LANGUAGE L1 LEFT OUTER JOIN LANGUAGE L2 ON (L1.name = L2.name)
WHERE L1.country = 'USA' AND L2.country = 'MEX' AND L2.NAME IN L1.name
ORDER BY L2.NAME ASC;

-- Autre possibilit� avec un with 
With tableLangue (name) AS (
  SELECT name
  FROM LANGUAGE 
  WHERE country = 'USA')
SELECT * -- language.name, language.percentage
FROM tableLangue LEFT OUTER JOIN Language ON (tableLangue.name = Language.name)
WHERE language.country = 'MEX'
ORDER BY language.name ASC;
