-- PB2 QUESTION 1
-- Vincent Falconieri; La�titia Lachat

--Question 2.1. Affichez toutes les langues parl�es pr�sentes dans la base, tri�es par ordre alphab�tique.
SELECT DISTINCT NAME
FROM LANGUAGE
ORDER BY NAME ASC;
