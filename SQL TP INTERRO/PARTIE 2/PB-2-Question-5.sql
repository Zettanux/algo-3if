-- PB2 QUESTION 5
-- Vincent Falconieri; La�titia Lachat

--Question 2.5. Recherchez les pays dans lesquels on parle au moins deux langues.
--Le r�sultat de la requ�te fera appara�tre deux attributs : le nom du pays, le nombre de langues parl�es.
--Les r�sultats seront affich�s par nombre de langues parl�es d�croissante.

With tableLangue (Code, nbLangues) AS (
SELECT COUNTRY, COUNT(NAME) as nbLangues
FROM LANGUAGE 
GROUP BY COUNTRY
HAVING COUNT(NAME) >2)
SELECT country.name, nbLangues as nbLangues
FROM COUNTRY JOIN tableLangue ON (Country.code = tableLangue.Code)
ORDER BY nbLangues DESC;

-- Le with juste pour r�cup�rer le nom apr�s la s�lection (exactement)