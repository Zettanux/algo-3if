-- PB1 QUESTION 7
-- Vincent Falconieri; La�titia Lachat

--Question 1.7. Donnez le code permettant que le nom d�un athl�te soit en majuscule.
CREATE OR REPLACE TRIGGER t_Athelete_name_upper
  BEFORE INSERT OR UPDATE
  ON ATHLETE
  FOR EACH ROW
  DECLARE
  
  BEGIN
    :NEW.nom := UPPER(:NEW.nom);
    
END;
/

-- Test pour v�rifier si �a fonctionne
SELECT * 
FROM Athlete;

INSERT INTO Athlete
VALUES (2,'jean-marie', 'canada');
