-- PB1 QUESTION 10
-- Vincent Falconieri; La�titia Lachat

--Question 1.10. Donner le code n�cessaire permettant de garantir qu�un Athl�te est bien inscrit � des �preuve correspondant
--� son genre.

-- On rajoute une contrainte
ALTER TABLE Athlete
ADD CONSTRAINT c_Athlete_Genre
CHECK (Genre= 'F' OR Genre = 'M');

-- Jeu de test
INSERT INTO Athlete
VALUES (15,'MARINE','FRANCE','F');

SELECT * 
FROM Athlete;

INSERT INTO Epreuve
VALUES (15,1,1,'EP15','M','INDIVIDUEL');

SELECT * 
FROM Epreuve;

INSERT INTO Medaille
VALUES (15,15,15);

SELECT * 
FROM Medaille;


