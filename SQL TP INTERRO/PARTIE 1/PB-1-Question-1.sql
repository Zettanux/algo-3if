-- PB1 QUESTION 1
-- Vincent Falconieri; La�titia Lachat

--Question 1.1. Donnez le code permettant de cr�er les tables correspondantes sous Oracle. Doivent figurer les contraintes
--correspondant aux choix des cl�s primaires ainsi que les contraintes r�f�rentielles.

-- CREATION DES TABLES
CREATE TABLE Athlete
(IdAthlete number CONSTRAINT AthleteKey PRIMARY KEY ,
Nom VARCHAR2(32),
PaysAppartenance VARCHAR2(50));

CREATE TABLE Medaille
(IdMedaille number CONSTRAINT MedailleKey PRIMARY KEY ,
IdAthlete number,
IdEpreuve number);

CREATE TABLE Epreuve
(IdEpreuve number CONSTRAINT EpreuveKey PRIMARY KEY ,
IdSport number,
IdJeux number,
Libelle VARCHAR2(32),
Genre VARCHAR2(32),
TypeClassement VARCHAR2(32));

CREATE TABLE Sport
(IdSport number CONSTRAINT SportKey PRIMARY KEY ,
Nom VARCHAR2(32));

CREATE TABLE Jeux
(IdJeux number CONSTRAINT JeuxKey PRIMARY KEY ,
PaysOrganisateur VARCHAR2(32),
Ville VARCHAR2(32),
Type VARCHAR2(32),
Annee number);

-- FOREIGN KEY

ALTER TABLE Epreuve
ADD CONSTRAINT fk_Jeux_Epreuve
FOREIGN KEY (IdJeux)
REFERENCES Jeux (IdJeux);

ALTER TABLE Epreuve
ADD CONSTRAINT fk_Sport_Epreuve
FOREIGN KEY (idSport)
REFERENCES Sport (idSport);

ALTER TABLE Medaille
ADD CONSTRAINT fk_Athlete_Medaille
FOREIGN KEY (IdAthlete)
REFERENCES Athlete (IdAthlete);

ALTER TABLE Medaille
ADD CONSTRAINT fk_Epreuve_Medaille
FOREIGN KEY (idEpreuve)
REFERENCES Epreuve (idEpreuve);


-- ID UNIQUE POUR SPORT
ALTER TABLE Sport
ADD CONSTRAINT u_Sport_Nom
UNIQUE (nom);

-- CHECK
ALTER TABLE Jeux
ADD CONSTRAINT c_Jeux_Annee
CHECK (Annee IS NOT NULL);

/* Jeux de donn�es personnel
INSERT INTO Athlete
VALUES (0,'JEAN','FRANCE');
INSERT INTO Medaille
VALUES (0,0,0);
INSERT INTO Epreuve
VALUES (0,1,1,'EP1','COURSE','PODIUM');
INSERT INTO Sport
VALUES (0,'cheval');
INSERT INTO Sport
VALUES (1,'course');
INSERT INTO Jeux
VALUES (1,'ALLEMAGNE','BERLIN','ETE',2000); */
