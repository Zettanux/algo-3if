-- PB1 QUESTION 8
-- Vincent Falconieri; La�titia Lachat

--Question 1.8. Nous souhaitons garder les logins des utilisateurs qui ont modifi� le r�sultat d�une comp�tition que ce soit
--par insertion, suppression ou mise � jour (conservation du log des op�rations). Donnez le code permettant de cr�er la
--table idoine, ainsi que le code permettant de la renseigner.


create or replace TRIGGER IDOINE_log_trigger
  AFTER 
    INSERT OR UPDATE OR DELETE 
  ON Epreuve
  FOR EACH ROW  
  DECLARE
      log_action$  IDOINE.action%TYPE;  
      idtrackMAX$  IDOINE.IDTRACK%TYPE;    
  BEGIN
  
  SELECT MAX(IDTRACK) INTO idtrackMAX$
  FROM IDOINE;
  
  -- SELON L'ETAT On choisi le tracking
    IF INSERTING THEN
    log_action$ := 'Insert';
    ELSIF UPDATING THEN
    log_action$ := 'Update';
    ELSIF DELETING THEN
    log_action$ := 'Delete';
    END IF;

-- On ins�re le log
    INSERT INTO IDOINE
    VALUES(idtrackMAX$+1, USER, TO_CHAR(SYSDATE, 'DD-MON-YY'), log_action$);
      
END;