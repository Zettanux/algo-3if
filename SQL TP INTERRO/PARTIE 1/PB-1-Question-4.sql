-- PB1 QUESTION 4
-- Vincent Falconieri; La�titia Lachat

--Question 1.4. Donnez le code permettant de limiter le domaine du type d�un Jeu. Le type d�un Jeu doit �tre ��t� ou 
--Hiver�.

ALTER TABLE Jeux
ADD CONSTRAINT c_Jeux_type
CHECK (type= 'ETE' OR type = 'HIVER');

-- TEST 
INSERT INTO Jeux
VALUES (0,'FRANCE','PARIS','BLABLA',2002);
INSERT INTO Jeux
VALUES (0,'FRANCE','PARIS','ETE',2002);
INSERT INTO Jeux
VALUES (2,'FRANCE','PARIS','HIVER',2002);
