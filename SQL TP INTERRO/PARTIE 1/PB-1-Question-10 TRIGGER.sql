-- PB1 QUESTION 8
-- Vincent Falconieri; La�titia Lachat

--Question 1.10. Donner le code n�cessaire permettant de garantir qu�un Athl�te est bien inscrit � des �preuve correspondant
--� son genre.

create or replace TRIGGER MEDAILLE_Genre_ATlEPR_trigger
  AFTER 
    INSERT OR UPDATE
  ON MEDAILLE
  FOR EACH ROW  
  DECLARE
      genre_athelte$  Athlete.Genre%TYPE;  
      genre_epreuve$  Epreuve.genre%TYPE;    
  BEGIN
  
  SELECT Genre INTO genre_athelte$
  FROM Athlete
  WHERE idAthlete = :NEW.idAthlete;
  
  SELECT Genre INTO genre_epreuve$
  FROM Epreuve
  WHERE idEpreuve = :NEW.idEpreuve;

  IF genre_athelte$ != genre_epreuve$ THEN
      RAISE_APPLICATION_ERROR(-20005,'GENRE INCOMPATIBLES');
      -- Il faut normalement cr�er une table avec les erreurs et faire une s�lection sur cette table. ;)
    END IF;
      
END;
/