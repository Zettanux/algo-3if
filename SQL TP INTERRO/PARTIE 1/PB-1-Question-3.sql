-- PB1 QUESTION 3
-- Vincent Falconieri; La�titia Lachat

--Question 1.3. Donnez le code permettant de limiter le domaine du classement. Le type de classement doit �tre �individuel�,
--ou ��quipe�.
ALTER TABLE Epreuve
ADD CONSTRAINT c_Epreuve_Type_Classement
CHECK (TypeClassement= 'INDIVIDUEL' OR TypeClassement = 'EQUIPE');

-- TEST 
INSERT INTO Epreuve
VALUES (3,1,1,'EP3','M','BLABLA');
INSERT INTO Epreuve
VALUES (3,1,1,'EP4','M','EQUIPE');
INSERT INTO Epreuve
VALUES (4,1,1,'EP5','M','INDIVIDUEL');
