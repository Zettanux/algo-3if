-- PB1 QUESTION 6
-- Vincent Falconieri; La�titia Lachat

--Question 1.6. Donnez le code permettant de d�terminer depuis quand une ville n�a pas accueilli de jeux. Pr�cisez les
--�ventuelles conventions.

-- On faitr la diff�rence avec la date actuelle du syst�me (ann�e)
SELECT to_number(to_char(sysdate,'RRRR')) - MAX(Annee)
FROM Jeux
WHERE ville = 'PARIS';

-- test
INSERT INTO Jeux
VALUES (10,'FRANCE','PARIS','ETE',1998);
