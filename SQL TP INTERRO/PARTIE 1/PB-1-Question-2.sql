-- PB1 QUESTION 2
-- Vincent Falconieri; La�titia Lachat

--Question 1.2. Donnez le code permettant de limiter le domaine du genre d�une �preuve. Le genre d�une �preuve doit �tre
--�M�, pour masculin, ou �F�, pour f�minin.
ALTER TABLE Epreuve
ADD CONSTRAINT c_Epreuve_Genre
CHECK (Genre= 'F' OR Genre = 'M');

-- TEST 
INSERT INTO Epreuve
VALUES (2,1,1,'EP3','BLABLA','PODIUM');
INSERT INTO Epreuve
VALUES (2,1,1,'EP3','M','PODIUM');