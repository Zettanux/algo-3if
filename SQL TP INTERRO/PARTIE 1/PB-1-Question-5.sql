-- PB1 QUESTION 5
-- Vincent Falconieri; La�titia Lachat

--Question 1.5. Donnez le code n�cessaire pour garantir que la nationalit� d�un athl�te soit connue.
ALTER TABLE Athlete
ADD CONSTRAINT c_Athlete_PaysAppartenance
CHECK (PaysAppartenance IS NOT NULL);

-- TEST
INSERT INTO Athlete
VALUES (1,'MICHEL',null);
INSERT INTO Athlete
VALUES (1,'MICHEL','');
INSERT INTO Athlete
VALUES (1,'MICHEL','ALLEMAGNE');
