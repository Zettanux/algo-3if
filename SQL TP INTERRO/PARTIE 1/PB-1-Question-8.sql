-- PB1 QUESTION 8
-- Vincent Falconieri; La�titia Lachat

--Question 1.8. Nous souhaitons garder les logins des utilisateurs qui ont modifi� le r�sultat d�une comp�tition que ce soit
--par insertion, suppression ou mise � jour (conservation du log des op�rations). Donnez le code permettant de cr�er la
--table idoine, ainsi que le code permettant de la renseigner.

CREATE TABLE IDOINE(
    idtrack NUMBER, 
    log_user VARCHAR2(30),
    log_date DATE,
    action VARCHAR2(30),
    CONSTRAINT IDOINE PRIMARY KEY (idtrack)
    );

-- CODE DE TEST
INSERT INTO Epreuve
VALUES (5,1,1,'EP6','M','INDIVIDUEL'); 

SELECT * 
FROM Epreuve;
SELECT * 
FROM IDOINE;

UPDATE Epreuve
SET GENRE = 'F'
WHERE IDEpreuve = 5;

SELECT * 
FROM Epreuve;
SELECT * 
FROM IDOINE;

DELETE 
FROM Epreuve
WHERE IDEpreuve = 5;

SELECT * 
FROM Epreuve;
SELECT * 
FROM IDOINE;