/* 
 * File:   main.cpp
 * Author: zettanux
 *
 * Created on 26 novembre 2016, 11:17
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main()
{
    const int nbCasesHistogramme = 21;
    //Stocke l'histogramme
    int listeHistogramme[nbCasesHistogramme];
    int max;

    //Récupération du tableau
    for (int i = 0; i < nbCasesHistogramme; i++)
    {
        cin >> listeHistogramme[i];
        if (i == 0)
        {
            max = listeHistogramme[i];
        }
        else if (max < listeHistogramme[i])
        {
            max = listeHistogramme[i];
        }
    }

    for (int i = 0; i < max; i++)
    {
        for (int j = 0; j < nbCasesHistogramme; j++)
        {
            //Si la hauteur de la case d'histogramme est plus haute que le seuil actuel
            if (listeHistogramme[j] >= (max - i))
            {
                printf("** ");
            }
            else // la haute de la pile est pas assez grande pour être affichée
            {
                printf("   ");
            }
            
        }
        printf("\r\n");
    }
    
    printf(" 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 \r\n");

    return 0;
}